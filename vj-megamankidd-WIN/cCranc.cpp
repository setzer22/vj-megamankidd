#include "cCranc.h"

cCranc::cCranc(vector<cEntity*>* entities_, cScene* scene_) {
  w = 22; h = 16;
  cont = -1; //el -1 representa que está viu.
  attack_anime = 0; //sense animació d'atac.
  frame = 120;//cada 120 frames disaprem una bubble.
  scene = scene_;
  entities = entities_;
}
cCranc::~cCranc() {}

void cCranc::draw()
{
    //Draws the sprite
    if(cont == -1 || cont%2 == 0){ //si la gamba no está morta, o está els frames parells de la seva mort es pinta, sino no.
    float xo,yo,xf,yf;
    xf = yf = 0;
    
    float sqy = 0.5f; 
    float sqx = 0.5f;
    
    if (attack_anime == 0) {
      w = 22; h = 16;
      xo = 0.0; yo = sqy;
      xf = xf + xo + sqx-0.05;
      yf = yf + yo - sqy+0.1;
    }
    else {
      w = 22; h = 22;
      xo = sqx-0.05; yo = sqy;
      xf = 1;
      yf = 0;
      --attack_anime;
    }
    
    int screen_x,screen_y;

    screen_x = x + SCENE_Xo;
    screen_y = y + SCENE_Yo;

    glEnable(GL_TEXTURE_2D);
    
    glBindTexture(GL_TEXTURE_2D,tex_id);
    glBegin(GL_QUADS);	
	    glTexCoord2f(xo,yo);	glVertex2i(screen_x  ,screen_y);
	    glTexCoord2f(xf,yo);	glVertex2i(screen_x+w,screen_y);
	    glTexCoord2f(xf,yf);	glVertex2i(screen_x+w,screen_y+h);
	    glTexCoord2f(xo,yf);	glVertex2i(screen_x  ,screen_y+h);
    glEnd();
    }
}
void cCranc::input(unsigned char* keys)
{
    //check for the keys and modify vx, vy
    //if it's an enemy, AI goes here
  
}
void cCranc::update()
{
    //update according to vx, vy. This blindly moves the character
    //and checks for collisons with the environment. Doesn't use AI
    if (cont == -1) {
      if (frame == 0) {
	shoot();
	frame = 100;
	attack_anime = 17;
      }
      else --frame;
    }
    else {
      if(cont == 0) destroy();
      else --cont;
    }
}
void cCranc::onCollision(cEntity* e)
{
    switch(e->getType()){
        case ENTITY_BULLET:
            if(cont == -1) dead();
            break;
        case ENTITY_PUNY:
            if(cont == -1 && static_cast<cPuny*>(e)->attacking) dead();
            break;
    }
}

void cCranc::dead()
{
  cont = 20;
  attack_anime = 0;
  points += 20;
}
void cCranc::shoot() {
  cBubble* bob = new cBubble(scene);
  bob->x = x+5; bob->y = y+25;
  bob->tex_id = tex_id;
  entities->push_back(bob);
  
}