SOURCES := $(filter-out %WIN.cpp,$(wildcard src/*.cpp))
HEADERS := $(wildcard src/*.h)
OBJECTS := $(patsubst src/%.cpp,obj/%.o, $(SOURCES))

CXX=clang++
CXX_FLAGS=-O0 -g -DGLIBCXX_DEBUG -Wall -std=c++11 -DLINUX

all: bin obj bin/main.x Makefile

bin:
	mkdir bin

obj:
	mkdir obj

bin/main.x: $(OBJECTS)
	@echo Compiling main executable
	$(CXX) $(CXX_FLAGS) -o bin/main.x $(OBJECTS) -lGL -lGLU -lglut -lSOIL
src/%.cpp: $(HEADERS) 

obj/%.o: src/%.cpp
	@echo Compiling $< into $@
	$(CXX) $(CXX_FLAGS) -I/usr/include -c $< -o $@

clean:
	rm obj/*.o bin/main.x

print-% : ; @echo $* = $($*)
