#pragma once

#include "Globals.h"
#include "cEntity.h"

#define ROCK 0
#define SCISSORS 1
#define PAPER 2

class cRPS : public cEntity {
public:
    cRPS();
    virtual ~cRPS();

    virtual void draw();
    virtual int getType();
    virtual void input(unsigned char* keys);
    virtual void onCollision(cEntity* e);
    virtual void update();
    void next();
    void previous();
    int get() {return state;}
    void set(int state_) {state = state_;}
    
    bool visible;
    
private:
    int state;
};