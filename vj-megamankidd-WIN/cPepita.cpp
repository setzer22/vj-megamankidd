#include "cPepita.h"

cPepita::cPepita()
{
  w = h = 14;
}

void cPepita::input(unsigned char* keys) {}

void cPepita::update() {}

void cPepita::draw()
{
    float xo = 0, yo = 1, yf = 0 , xf = 1;
    float screen_x = x+SCENE_Xo, screen_y = y+SCENE_Yo;
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D,tex_id);
    glBegin(GL_QUADS);  
	glTexCoord2f(xo,yo);    glVertex2i(screen_x  ,screen_y);
	glTexCoord2f(xf,yo);    glVertex2i(screen_x+w,screen_y);
	glTexCoord2f(xf,yf);    glVertex2i(screen_x+w,screen_y+h);
	glTexCoord2f(xo,yf);    glVertex2i(screen_x  ,screen_y+h);
    glEnd();
}


void cPepita::onCollision(cEntity* e)
{
  if(e->getType() == ENTITY_PLAYER) {
    points += 10;
    cout << "points: " << points << endl;
    destroy();
  }
}

int cPepita::getType()
{
  return ENTITY_PEPITA;
}