#include "cPowerup.h"

cPowerup::cPowerup()
{
    w = h = 16;
    player = NULL;
}
void cPowerup::draw()
{
    float xo = 0, yo = 1, yf = 0 , xf = 1;
    float screen_x = x+SCENE_Xo, screen_y = y+SCENE_Yo;
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D,tex_id);
    glBegin(GL_QUADS);  
    glTexCoord2f(xo,yo);    glVertex2i(screen_x  ,screen_y);
    glTexCoord2f(xf,yo);    glVertex2i(screen_x+w,screen_y);
    glTexCoord2f(xf,yf);    glVertex2i(screen_x+w,screen_y+h);
    glTexCoord2f(xo,yf);    glVertex2i(screen_x  ,screen_y+h);
    glEnd();
}
int cPowerup::getType()
{
  return ENTITY_POWERUP;
}
void cPowerup::update() {
    if(player) {
        int dx = (player->x + 4) - x;
        int dy = (player->y + 4) - y;
        
        if(dx != 0) x += (dx>=0) ? max(1,dx/30) : min(-1,dx/30);
        if(dy != 0) y += (dy>=0) ? max(1,dy/30) : min(-1,dy/30);
        
        //Sometimes dx and dy will never be 0 so we check within a boundary
        if(dx == 0 && dy == 0) destroy();
    }
}

void cPowerup::input(unsigned char* keys) {}
void cPowerup::onCollision(cEntity* e) {
    if(e->getType() == ENTITY_PLAYER) player = e;
}
