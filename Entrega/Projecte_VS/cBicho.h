#pragma once

#include <cassert>
#include "cTexture.h"
#include "Globals.h"

#include "cTile.h"
#include "cEntity.h"
#include "cBullet.h"
#include "cGamba.h"
#include "cCranc.h"
#include "cPuny.h"
#include "cRPS.h"

#include "cSoundManager.h"

#include <vector>

#define FRAME_DELAY		8
#define STEP_LENGTH		2
#define JUMP_HEIGHT		96
#define JUMP_STEP		3   
#define PUNCH_OFFSET    9

#define STATE_LOOKLEFT		0
#define STATE_LOOKRIGHT		1
#define STATE_WALKLEFT		2
#define STATE_WALKRIGHT		3
#define STATE_PUNCHLEFT		4
#define STATE_PUNCHRIGHT	5
#define STATE_JUMPLEFT		6
#define STATE_JUMPRIGHT		7
#define STATE_SHOOTLEFT		8
#define STATE_SHOOTRIGHT	9
#define STATE_FANFARE		10
#define STATE_DEAD		11
#define STATE_JUMPPUNCHLEFT	12
#define STATE_JUMPPUNCHRIGHT	13
#define STATE_JUMPSHOOTLEFT	14
#define STATE_JUMPSHOOTRIGHT	15

#ifdef LINUX  //Windows doesn't even support this kind of declaration...
//    string cPlayerStateToString[16] = {
        //"LOOKLEFT", "LOOKRIGHT", "WALKLEFT", "WALKRIGHT", "PUNCHLEFT", "PUNCHRIGHT",
        //"JUMPLEFT", "JUMPRIGHT", "SHOOTLEFT", "SHOOTRIGHT", "FANFARE", "DEAD", "JUMPPUNCHLEFT",
        //"JUMPPUNCHRIGHT", "JUMPSHOOTLEFT", "JUMPSHOOTRIGHT"
    //}; 
#endif


class cRect
{
public:
	int left,top,
		right,bottom;
};

class cBicho : public cEntity
{
public:
	cBicho(vector<cEntity*>* entities, cScene* scene_);
	cBicho(int x,int y,int w,int h, cScene* scene_);
	~cBicho(void);

	void SetPosition(int x,int y);
	void GetPosition(int *x,int *y);
	void SetTile(int tx,int ty);
	void GetTile(int *tx,int *ty);
	void SetWidthHeight(int w,int h);
	void GetWidthHeight(int *w,int *h);
    void updatePuny();
    bool isStateRight();

	bool Collides(cRect *rc);
	bool CollidesMapWall(cTile* map,bool right);
	bool CollidesMapFloor(cTile* map);
    bool CollidesMapCeiling(cTile* map);
    bool PunchCollisions(cTile* map, bool right, bool jumping);
	void GetArea(cRect *rc);
	void DrawRect(float xo,float yo,float xf,float yf);
    void ShotBullet(cTile* map);

	void MoveRight(cTile *map);
	void MoveLeft(cTile *map);
	void Jump(cTile *map);
	void MoveDown(cTile *map);
	void Atack(cTile *map);
	void Stop();
	void Logic(cTile *map);

	int  GetState();
	void SetState(int s);

	void NextFrame(int max);
	int  GetFrame();
    
    void setOnWater(bool value);
    bool getOnWater() {return onWater;}
    
    void input(unsigned char* keys) override; //Handles the input / IA
    void inputBossfight(unsigned char* keys);
    void update() override; //Handles the logic
    void updateBossfight();
    void setRPS(cRPS* rps) {this->rps = rps;}
    virtual void onCollision(cEntity* e) override;
    virtual int getType() override { return ENTITY_PLAYER; }
	
	int GetCont();
    bool GetBossfight() {return bossfight;}
    int GetDeadCont() {return dead_cont;}
    void SetDeadCont(int dc) {dead_cont = dc;}
    bool GetShootMode() {return shoot_mode;}
	
private:
	int state;

    bool last_l, last_r;
	bool jumping;
    int ceil_length;
	int jump_alfa;
	int jump_y;
    int moveSpeed, jumpSpeed;
    
    float xf, waterSpeed, acceleration, yf;
    bool onWater;
	
	int cont, bossfight_cont, dead_cont;

	int seq,delay;
    
    bool lastX, lastC;
    bool shoot_mode, floating, bossfight, selecting_rps, mode;
    
    cScene* scene;
    cPuny* puny; //Owned by the class
    cRPS* rps;
    vector<cEntity*>* entities;
};

