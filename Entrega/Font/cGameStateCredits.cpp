#include "cGameStateCredits.h"

cGameStateCredits::cGameStateCredits(std::vector< cEntity* >* entities, unsigned char* keys): cGameState(entities, keys)
{
    this->entities = entities;
    this->keys = keys;
    count = 0;
}
cGameStateCredits::~cGameStateCredits()
{
}
bool cGameStateCredits::Init() {
    bool res = true;
    res = cData::getInstance()->LoadImage(IMG_CREDITS, "credits.png", GL_RGBA);
    if(!res) return false;
    return res;
}
bool cGameStateCredits::Loop()
{
    glutPostRedisplay();
    Process();
    return true;
}
bool cGameStateCredits::Process()
{
    if(keys[27] || keys[13]) {
        keys[27] = false;
        keys[13] = false;
        cSoundManager::getInstance()->StopSound("menu");
        cGame::ChangeState(GAMESTATE_MENU);
    }
    return true;
}
void cGameStateCredits::Render()
{
    cout << "rendering" << endl;
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, cData::getInstance()->GetID(IMG_CREDITS));
    glBegin(GL_QUADS);
        glTexCoord2f(0,1); glVertex3f(0,0,0);
        glTexCoord2f(0,0); glVertex3f(0, GAME_HEIGHT, 0);
        glTexCoord2f(1,0); glVertex3f(GAME_WIDTH, GAME_HEIGHT, 0);
        glTexCoord2f(1,1); glVertex3f(GAME_WIDTH, 0, 0);
    glEnd();
    glutSwapBuffers();
}

