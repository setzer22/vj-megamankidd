#pragma once

#include "Globals.h"
#include "cEntity.h"
#include "cPepita.h"
#include "cData.h"
#include "cPowerup.h"
#include "cDyingBlock.h"

class cTile {
public:

    cTile();

	cTile(int tile_id_, int x_, int y_, std::vector<cEntity*>* entities);

	bool onAttack();

    bool collider;
    bool ceiling;
    bool harmful;
    bool destroyable;
    bool has_points;
    //Two bools more fit here!  
    int tex_id, tile_id, x, y;
    
    std::vector<cEntity*>* entities;
    
    cData* Data;
   
};