#include "Globals.h"
#include "cTexture_WIN.h"
#include <SOIL/SOIL.h>

#include <iostream>
using namespace std;

cTexture::cTexture(void)
{
}

cTexture::~cTexture(void)
{
}

bool cTexture::Load(char *filename,int type,int wraps,int wrapt,int magf,int minf,bool mipmap)
{
    unsigned char* data;
	int components;

    //TODO check if file exists!

	if(type!=GL_RGB && type !=GL_RGBA) return false;

    data = SOIL_load_image 
        (
            filename,
            &width, &height, &components,
            (type==GL_RGB)?SOIL_LOAD_RGB:SOIL_LOAD_RGBA 
        );

    cout << "[CTEXTURE] - Loaded texture " <<filename << " (" <<  width << "x" << height << ", " << ((components==3)?"RGB)":"RGBA)") << endl;

	glGenTextures(1, &id);
	glBindTexture(GL_TEXTURE_2D,id);

	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,wraps);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,wrapt);

	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,magf);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,minf);

	if(!mipmap)
	{
		glTexImage2D(GL_TEXTURE_2D,0,components,width,height,0,type,
					 GL_UNSIGNED_BYTE,data);
	}
	else
	{
		gluBuild2DMipmaps(GL_TEXTURE_2D,components,width,height,type,
						  GL_UNSIGNED_BYTE,data);
	}

	return true;
}
int cTexture::GetID()
{
	return id;
}
void cTexture::GetSize(int *w,int *h)
{
	*w = width;
	*h = height;
}
