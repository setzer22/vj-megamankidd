#include "cBullet.h"

cBullet::cBullet(cScene* scene_) {
    w = 8;
    h = 6;
    speed = 3;
    scene = scene_;
}

cBullet::~cBullet() {}

void cBullet::draw() {
    float sqx = 0.125f, sqy = 0.125f;
    float xo = 2*sqx, yo = 8*sqy, yf = 8*sqy - .03125f, xf = 2*sqx + .041666f;
    float screen_x = x+SCENE_Xo, screen_y = y+SCENE_Yo;
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D,tex_id);
    glBegin(GL_QUADS);  
        glTexCoord2f(xo,yo);    glVertex2i(screen_x  ,screen_y);
        glTexCoord2f(xf,yo);    glVertex2i(screen_x+w,screen_y);
        glTexCoord2f(xf,yf);    glVertex2i(screen_x+w,screen_y+h);
        glTexCoord2f(xo,yf);    glVertex2i(screen_x  ,screen_y+h);
    glEnd();
}

bool cBullet::collidesWithMap() {
    int tx = (x + (right?w:0) ) / TILE_SIZE, ty = y / TILE_SIZE;
    cTile* map = scene->GetMap();
    if(map[ tx + (ty*SCENE_WIDTH) ].collider) {
        bool needsRefresh = map[ tx + (ty*SCENE_WIDTH) ].onAttack();
        if(needsRefresh) scene->UpdateDisplay();
        return true;   
    }
    return false;
}

void cBullet::update() {
    x += (right?1:-1) * speed;
    if(collidesWithMap()) destroy();
    if(x < -10 || x > SCENE_WIDTH * TILE_SIZE) destroy();
}

void cBullet::onCollision(cEntity* e) {
    if( e->getType() != ENTITY_PLAYER &&
        e->getType() != ENTITY_NAVI &&
        e->getType() != ENTITY_NAVI_HITBOX &&
        e->getType() != ENTITY_PUNY &&
        e->getType() != ENTITY_PEPITA &&
        e->getType() != ENTITY_POWERUP) {
        destroy();
    }
}
