#include "Globals.h"
#include "cEntity.h"
#include "cSoundManager.h"
#include "cCranc.h"
#include "cGamba.h"

#define NAVI_PLAYER 0
#define NAVI_FLYINGTO 2
#define NAVI_GOINGBACK 3

using namespace std;

//This is a helper class that has to go before the main class
//because C++.
class cNaviHitbox : public cEntity {
public:
    cNaviHitbox();
    virtual ~cNaviHitbox();
    
    virtual void draw();
    virtual int getType();
    virtual void input(unsigned char* keys);
    virtual void onCollision(cEntity* e);
    virtual void update();
    
    vector<cEntity*> inReach; //Very poor approach
};

class cNavi : public cEntity {
public:
    cNavi(cEntity* player, vector<cEntity*>* entities);
    virtual ~cNavi();
    
    virtual void draw();
    virtual int getType();
    virtual void input(unsigned char* keys);
    virtual void onCollision(cEntity* e);
    virtual void update();
    void ChangeState(int newState);
    void setXY(int x, int y) {this->x = x; this->y = y; xf = x; yf = y;}
    bool checkIfDead(cEntity* e);
private:
    cEntity* player;
    cEntity* target;
    cNaviHitbox* hitbox;
    int count;
    int state;
    int animState;
    float fly_speed;
    
    float xf, yf, vx, vy, ax, ay;
};
