#include "Globals.h"
#include "cEntity.h" 
#include "cData.h"

class cDyingBlock : public cEntity{
public:
    cDyingBlock(int tile_id);
    virtual ~cDyingBlock();
    
    virtual void draw();
    virtual int getType() {return ENTITY_DYINGBLOCK;};
    virtual void input(unsigned char* keys) {}
    virtual void onCollision(cEntity* e) {}
    virtual void update() {}
    
private:
    int tile_id;
    int frame;
};