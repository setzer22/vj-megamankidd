#include "cPlayer.h"
#include <iostream>

cPlayer::cPlayer(vector<cEntity*>* entities, cScene* scene) : cBicho(entities, scene) {}
cPlayer::~cPlayer(){}

void cPlayer::draw()
{	
	float xo,yo,xf,yf;
	xf = yf = 0;
    
    float sqy = 0.125f; 
    float sqx = 0.125f;

	switch(GetState())
	{
		case STATE_LOOKLEFT:
		    SetWidthHeight(24,24);
		    xo = 0.0f; yo = 2*sqy;
		    break;
		case STATE_LOOKRIGHT:
		    SetWidthHeight(24,24);
		    xo = 0.0f; yo = sqy;
		    break;
		case STATE_WALKLEFT:
		    SetWidthHeight(24,24);
		    xo = sqx + (GetFrame()*sqx); yo = 2*sqy;
		    NextFrame(3);
		    break;
		case STATE_WALKRIGHT:
		    SetWidthHeight(24,24);
		    xo = sqx + (GetFrame()*sqx); yo = sqy;
		    NextFrame(3);
		    break;
		case STATE_PUNCHLEFT:
		    SetWidthHeight(30,24);
		    xo = 2*sqx; yo = 3*sqy;
		    xf = 0.04687f; 
		    break;
		case STATE_PUNCHRIGHT:
		    SetWidthHeight(30,24);
		    xo = 0.0f; yo = 3*sqy;
		    xf = 0.04687f;
		    break;
		case STATE_JUMPLEFT:
		    SetWidthHeight(24,30);
		    xo = 3*sqx; yo = 5*sqy;
		    yf = -0.03125;
		    break;
		case STATE_JUMPRIGHT:
		    SetWidthHeight(24,30);
		    xo = 0.0f; yo = 5*sqy;
		    yf = -0.03125;
		    break;
		case STATE_SHOOTLEFT:
		    SetWidthHeight(30,24);
		    xo = 1*sqx; yo = 5*sqy;
		    xf = 0.03654f; 
		    break;
		case STATE_SHOOTRIGHT:
		    SetWidthHeight(30,24);
		    xo = 1*sqx; yo = 4*sqy;
		    xf = 0.03654f; 
		    break;
		case STATE_FANFARE: {
		    SetWidthHeight(24,30);
		    int frame = GetFrame();
		    xo = sqx*frame; yo = (frame==0)? 5*sqy : 7*sqy;
		    yf = -0.062555f;
		    NextFrame(4); 
		    break;
		}
		case STATE_DEAD:
		    SetWidthHeight(30,24);
		    xo = 0*sqx; yo = 8*sqy;
		    xf = 0.05208333f; 
		    break;
		case STATE_JUMPPUNCHLEFT:
		    SetWidthHeight(30,30);
		    xo = 4*sqy; yo = 4*sqy ;
		    xf = 0.03125; 
            yf = -0.03125;
		    break;
		case STATE_JUMPPUNCHRIGHT:
		    SetWidthHeight(30,30);
		    xo = 6*sqx; yo = 2*sqy;
		    xf = 0.03125;
            yf = -0.03125;
		    break;
		case STATE_JUMPSHOOTLEFT:
		    SetWidthHeight(30,30);
		    xo = 6*sqx; yo = 4*sqy;
		    xf = 0.03125; 
		    yf = -0.03125;
		    break;
		case STATE_JUMPSHOOTRIGHT:
		    SetWidthHeight(30,30);
		    xo = 4*sqx; yo = 2*sqy;
		    xf = 0.03125;
		    yf = -0.03125;
		    break;

	}
	xf = xf + xo + sqx;
	yf = yf + yo - sqy;
    
    DrawRect(xo, yo, xf, yf);
}
