#include "cGame.h"
#include "Globals.h"

#include <GL/gl.h>
#include <GL/glut.h>

vector<cEntity*> cGame::entities;
int cGame::state;
unsigned char cGame::keys[256];
cGameState* cGame::level1State;
cGameState* cGame::menuState;
cGameState* cGame::level2State;
cGameState* cGame::creditsState;
cGameState* cGame::helpState;
cGameState* cGame::gameOver;

cGame::cGame(void)
{
    entities.reserve(MAX_ENTITIES);
    state = GAMESTATE_MENU;
    level1State = new cGameStatePlay(&entities, keys);
    level2State = new cGameStatePlay2(&entities, keys);
    menuState = new cGameStateMenu(&entities, keys);
    creditsState = new cGameStateCredits(&entities, keys);
    helpState = new cGameStateHelp(&entities, keys);
    gameOver = new cGameStateGameOver(&entities, keys);
}

cGame::~cGame(void)
{
}

void cGame::ChangeState(int state_, int level)
{
    for(int i = 0; i < entities.size(); ++i) delete entities[i];
    entities.clear();
    
    if(state_ == GAMESTATE_GAMEOVER) static_cast<cGameStateGameOver*>(gameOver)->setLevel(level);
    
    state = state_;
    Init(); 
}

bool cGame::Init()
{
    //Graphics initialization
    glClearColor(0.4f,0.5f,0.8f,0.0f);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0,GAME_WIDTH,0,GAME_HEIGHT,0,1);
    glMatrixMode(GL_MODELVIEW);
    
    glAlphaFunc(GL_GREATER, 0.05f);
    glEnable(GL_ALPHA_TEST); 

    bool res = true;
    switch(state) {
    case GAMESTATE_GAME: {
        res = level1State->Init();
        return res;
    }
    case GAMESTATE_LEVEL2: {
        res = level2State->Init();
        return res;
    }
    case GAMESTATE_MENU: {
        res = menuState->Init();
        return res;
    }
    case GAMESTATE_CREDITS: {
        res = creditsState->Init();
        return res;
    }
    case GAMESTATE_INSTR: {
        res = helpState->Init();
        return res;
    }
    case GAMESTATE_GAMEOVER: {
	res = gameOver->Init();
	return res;
    }
    default: break;
    
    }
    
	return res;
}

bool cGame::Loop()
{
    bool res=true;
    switch (state) {
        
    case GAMESTATE_GAME:
        res = level1State->Loop();
        return res;
        
    case GAMESTATE_LEVEL2:
        res = level2State->Loop();
        return res;
        
    case GAMESTATE_MENU:
        res = menuState->Loop();
        return res;
        
    case GAMESTATE_INSTR:
        res = helpState->Loop();
        return res;
        
    case GAMESTATE_CREDITS:
        res = creditsState->Loop();
        return res;
    case GAMESTATE_GAMEOVER:
	res = gameOver->Loop();
	return res;
    }
    
    return res;
}
void cGame::Render()
{
    switch(state) {
    case GAMESTATE_GAME:
        level1State->Render();
        break;
        
    case GAMESTATE_LEVEL2:
        level2State->Render();
        break;
        
    case GAMESTATE_MENU:
        menuState->Render();
        break;
        
    case GAMESTATE_INSTR:
        helpState->Render();
        break;
        
    case GAMESTATE_CREDITS:
        creditsState->Render();
        break;
	
    case GAMESTATE_GAMEOVER:
	gameOver->Render();
	break;
    }

}

void cGame::Finalize()
{
}

//Input
void cGame::ReadKeyboard(unsigned char key, int x, int y, bool press)
{
	keys[key] = press;
}

void cGame::ReadMouse(int button, int state, int x, int y)
{
}

void cGame::drawRect(cEntity* e)
{
	glDisable(GL_TEXTURE_2D);
    glBegin(GL_QUADS);
	    glVertex2i(e->x,e->y);
	    glVertex2i(e->x+e->w,e->y);
	    glVertex2i(e->x+e->w,e->y+e->h);
	    glVertex2i(e->x,e->y+e->h);
    glEnd();
}

