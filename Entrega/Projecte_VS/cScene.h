#pragma once

#include "cTexture.h"
#include "cTile.h"
#include "cEntity.h"
#include <string>
#include <vector>
using namespace std;

#define SCENE_Xo		(2*TILE_SIZE)
#define SCENE_Yo		TILE_SIZE

#define SCENE_WIDTH    100
#define SCENE_HEIGHT   100

#define FILENAME		"level"
#define FILENAME_EXT	".txt"

#define TILE_SIZE		16
#define BLOCK_SIZE	    16	

#define ANIM_TIME	12

#define ROCK_TILE		13


class cScene
{
public:
	cScene(vector< cEntity* >* entities);
	virtual ~cScene();

	bool LoadLevel(int level);
	void Draw(int tex_id);
    void UpdateDisplay();
	void DrawAnimated();
    void DrawBackground(int tex_id);
    cTile makeTile(int tile_id, int level, int i, int j);
    bool isAnimated(int tile_id, int level);
	cTile* GetMap();

	int parseInt(string& s) {
		try {
			int i = 0, x = 0; bool neg = false;
			if(s[i] == '-') {neg = true; ++i;} 
			while(s[i] >= '0' && s[i] <= '9') {
				x = 10*x + (s[i]-'0');
				++i;
			}
			while(s[i] == ',') ++i;
			s = s.substr(i, s.length());
			return neg?-x:x;
		}
		catch (exception e) {
			return 0;
		}
		
	}
	
	int levelWater;

private:
	vector<cTile> map; //scene
	int id_DL;  //actual level display list
	int anim;
    int level;
    vector<cEntity*>* entities;
	
};
