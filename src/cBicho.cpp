#include "cBicho.h"
#include "cScene.h"
#include "Globals.h"
#include <iostream>

cBicho::cBicho(vector<cEntity*>* entities_, cScene* scene_)
{
    scene = scene_;
    entities = entities_;
	seq=0;
	delay=0;
    
    onWater = false;
    moveSpeed = STEP_LENGTH;
    jumpSpeed = JUMP_STEP;
    
	jumping = shoot_mode = false;
    ceil_length = -1;
    
    waterSpeed = 0;
    acceleration = 0.1;
    floating = false;
    
    puny = new cPuny();
    entities->push_back(static_cast<cEntity*>(puny));
    updatePuny();
    
    bossfight = mode = false;
    bossfight_cont = 0;
    dead_cont = -1;
    selecting_rps = false;
    last_l = false;
    last_r = false;

}
cBicho::~cBicho(){
    //puny will get freed because it's in the entity vector
}
bool cBicho::isStateRight()
{
    return state == STATE_WALKRIGHT || state == STATE_LOOKRIGHT ||
            state == STATE_JUMPRIGHT || state == STATE_JUMPSHOOTRIGHT ||
            state == STATE_JUMPPUNCHRIGHT || state == STATE_PUNCHRIGHT; 
}

void cBicho::updatePuny() {
    if(isStateRight()) {puny->x = x + 18;}
    else {puny->x = x-8;}
    puny->y = y + 6;
    puny->w = 14;
    puny->h = 14;
}

cBicho::cBicho(int posx,int posy,int width,int height, cScene* scene_)
{
	x = posx;
	y = posy;
	w = width;
	h = height;
	scene = scene_;
	xf = x;
    updatePuny();
}
void cBicho::input(unsigned char* keys)
{
    if(bossfight) {
        inputBossfight(keys);
        return;
    }
    
    if(!onWater && y / TILE_SIZE < scene->levelWater) setOnWater(true);
    else if(onWater && y / TILE_SIZE >= scene->levelWater) setOnWater(false);
    
    if(state == STATE_DEAD && dead_cont > -1) --dead_cont;
    
    if(keys[GLUT_KEY_UP] && !onWater && dead_cont == -1) Jump(scene->GetMap());
    else if(keys[GLUT_KEY_DOWN]&& dead_cont == -1) MoveDown(scene->GetMap());
    
    if(keys[GLUT_KEY_LEFT]&& dead_cont == -1)     MoveLeft(scene->GetMap());
    else if(keys[GLUT_KEY_RIGHT]&& dead_cont == -1)   MoveRight(scene->GetMap());
    else Stop();
    
    //Attack:
    if(!lastX && keys[GLUT_KEY_X]&& dead_cont == -1){
      Atack(scene->GetMap());
      puny->attacking = true;
      lastX = true;
    }
    lastX = keys[GLUT_KEY_X];
    
    if(!lastC && keys[GLUT_KEY_C]&& dead_cont == -1) {
      if(mode) shoot_mode = !shoot_mode;
      lastC = true;
    }
    lastC = keys[GLUT_KEY_C];
}
void cBicho::update()
{
    if(bossfight) updateBossfight();
    
    if (onWater && y / TILE_SIZE < 17) {
        //Flotem.
        if(floating && y/TILE_SIZE == 16) {
            floating = false;
        }
        else if(!floating && y/TILE_SIZE == 14) {
            floating = true;
        }

        if(floating && yf == 1) {
            int yaux = y;
            y += moveSpeed;
            if(CollidesMapCeiling(scene->GetMap())) {
                y = yaux;
            }
        }
        else if(floating && yf < 1) yf += 0.5;//Suavitzem l'efecte de flotar a la superficie.
        else if(!floating && yf > -1) yf -= 0.5;
        else y -= moveSpeed;
        
	switch(state) {
	  case STATE_LOOKLEFT:
	    state = STATE_JUMPLEFT;
	    break;
	  case STATE_LOOKRIGHT:
	    state = STATE_JUMPRIGHT;
	    break;
	  case STATE_PUNCHLEFT:
	    state = STATE_JUMPPUNCHLEFT;
	    break;
	  case STATE_PUNCHRIGHT:
	    state = STATE_JUMPPUNCHRIGHT;
	    break;
	  case STATE_SHOOTLEFT:
	    state = STATE_JUMPSHOOTLEFT;
	    break;
	  case STATE_SHOOTRIGHT:
	    state = STATE_JUMPSHOOTRIGHT;
	    break;
	}
    }
    else Logic(scene->GetMap());
    
    x = xf;
    updatePuny();
    if(state != STATE_PUNCHLEFT && state != STATE_PUNCHRIGHT &&
       state != STATE_JUMPPUNCHLEFT && state != STATE_JUMPPUNCHRIGHT) puny->attacking = false;
    
}


void cBicho::SetPosition(int posx,int posy)
{
	x = posx;
	y = posy;
	xf = x;
}
void cBicho::GetPosition(int *posx,int *posy)
{
	*posx = x;
	*posy = y;
}
void cBicho::SetTile(int tx,int ty)
{
	x = tx * TILE_SIZE;
	y = ty * TILE_SIZE;
	xf = x;
}
void cBicho::GetTile(int *tx,int *ty)
{
	*tx = x / TILE_SIZE;
	*ty = y / TILE_SIZE;
}
void cBicho::SetWidthHeight(int width,int height)
{
	w = width;
	h = height;
}
void cBicho::GetWidthHeight(int *width,int *height)
{
	*width = w;
	*height = h;
}
void cBicho::inputBossfight(unsigned char* keys)
{
    if(selecting_rps) {
        if(!last_l && keys[GLUT_KEY_LEFT]) {rps->previous();}
        last_l = keys[GLUT_KEY_LEFT];
        if(!last_r && keys[GLUT_KEY_RIGHT]) {rps->next();}
        last_r = keys[GLUT_KEY_RIGHT];
    }
}
void cBicho::updateBossfight()
{
    state = STATE_LOOKRIGHT; //I don't quite know why this works...
    ++bossfight_cont;
    if(bossfight_cont > 500) {
        assert(rps != NULL);
        selecting_rps = true;
    }
}
bool cBicho::Collides(cRect *rc)
{
	return ((x>rc->left) && (x+w<rc->right) && (y>rc->bottom) && (y+h<rc->top));
}

bool cBicho::CollidesMapWall(cTile* map,bool right)
{
    int tile_x,tile_y;
    int j;
    int width_tiles,height_tiles;

    if (right) 
        tile_x = (x+6) / TILE_SIZE;
    else 
        tile_x = x / TILE_SIZE;
        tile_y = y / TILE_SIZE;
        width_tiles  = w / TILE_SIZE;
        height_tiles = h / TILE_SIZE + (h % TILE_SIZE != 0);
    
        if(right)       tile_x += width_tiles;
        
        for(j=0;j<height_tiles;j++)
        {
		if(map[ tile_x + ((tile_y+j)*SCENE_WIDTH) ].harmful && dead_cont == -1) {dead_cont = 50; state = STATE_DEAD;}
                if(map[ tile_x + ((tile_y+j)*SCENE_WIDTH) ].collider)   return true;
        }
        
        return false;
}


bool cBicho::PunchCollisions(cTile* map, bool right, bool jumping)
{
    int tile_y = (y+(jumping?19:9)) / TILE_SIZE;
    int tile_x = (right? x+w:x-PUNCH_OFFSET) / TILE_SIZE;
    
    bool dirty = map[tile_x + (tile_y*SCENE_WIDTH)].onAttack();
    if(dirty) scene->UpdateDisplay();
    
    return true;
}


bool cBicho::CollidesMapFloor(cTile* map)
{
     int tile_x,tile_y;
        int width_tiles;
        bool on_base;
        int i;

        tile_x = x / TILE_SIZE;
        tile_y = y / TILE_SIZE;

        width_tiles = w / TILE_SIZE;
        if( (x % TILE_SIZE) != 0) width_tiles++;

        on_base = false;
        i=0;
        while((i<width_tiles) && !on_base)
        {
                if( (y % TILE_SIZE) == 0 )
                {
                        if(map[ (tile_x + i) + ((tile_y - 1) * SCENE_WIDTH) ].collider)
                                on_base = true;
			if(map[ (tile_x + i) + ((tile_y - 1) * SCENE_WIDTH) ].harmful && dead_cont == -1){//si colisionem amb una bomba morim.
				dead_cont = 50;
				state = STATE_DEAD;
			}
                }
                else
                {
                        if(map[ (tile_x + i) + (tile_y * SCENE_WIDTH) ].collider)
                        {
                                y = (tile_y + 1) * TILE_SIZE;
                                on_base = true;
                        }
                        if(map[ (tile_x + i) + (tile_y * SCENE_WIDTH) ].harmful && dead_cont == -1){//si colisionem amb una bomba morim.
				dead_cont = 50;
				state = STATE_DEAD;
			}
                }
                i++;
        }
        return on_base;
}

bool cBicho::CollidesMapCeiling(cTile* map)
{
    int tile_x,tile_y;
    int width_tiles;
    bool on_base;
    int i;

    tile_x = x / TILE_SIZE;
    tile_y = (y+h) / TILE_SIZE;

    width_tiles = w / TILE_SIZE;
    if( (x % TILE_SIZE) != 0) width_tiles++;

    on_base = false;
    i=0;
    while((i<width_tiles) && !on_base)
    {
        if( (y+w % TILE_SIZE) == 0 )
        {
            if(map[ (tile_x + i) + ((tile_y + 1) * SCENE_WIDTH) ].ceiling)
                on_base = true;
	    if(map[ (tile_x + i) + ((tile_y+1)*SCENE_WIDTH) ].harmful && dead_cont == -1){ //si colisionem contra una bomba morim.
	      dead_cont = 50;
	      state = STATE_DEAD;
	    }
        }
        else
        {
            if(map[ (tile_x + i) + (tile_y * SCENE_WIDTH) ].ceiling)
            {
                //y = (tile_y - 1) * TILE_SIZE;
                on_base = true;
            }
            if(map[ (tile_x + i) + (tile_y*SCENE_WIDTH) ].harmful && dead_cont == -1) {
	      dead_cont = 50;
	      state = STATE_DEAD;
	    }
        }
        i++;
    }
    return on_base;

}

void cBicho::onCollision(cEntity* e)
{
    cGamba* g; cCranc* c;
    switch(e->getType()) {
        case ENTITY_GAMBA:
            g = static_cast<cGamba*>(e);
            if (g->cont == -1) {
		if(state != STATE_DEAD) {
		  dead_cont = 50;
		  SetState(STATE_DEAD);
		}
            }
            break;
        case ENTITY_CRANC:
            c = static_cast<cCranc*>(e);
            if(c->cont == -1) {
	      if(state != STATE_DEAD) {
		  dead_cont = 50;
		  SetState(STATE_DEAD);
		} 
            }
            break;
        case ENTITY_BUBBLE:
	    if(state != STATE_DEAD) {
		  dead_cont = 50;
		  SetState(STATE_DEAD);
	  }
            break;
        case ENTITY_HITBOX: 
            cout << "Collided with hitbox" << endl;
            bossfight = true;
            break;
        case ENTITY_POWERUP:
	    mode = true;
            shoot_mode = true;
            break;
      }
}

void cBicho::GetArea(cRect *rc)
{
	rc->left   = x;
	rc->right  = x+w;
	rc->bottom = y;
	rc->top    = y+h;
}
void cBicho::DrawRect(float xo,float yo,float xf,float yf)
{
	int screen_x,screen_y;

	screen_x = x + SCENE_Xo;
	screen_y = y + SCENE_Yo + (BLOCK_SIZE - TILE_SIZE);

	glEnable(GL_TEXTURE_2D);
	
	glBindTexture(GL_TEXTURE_2D,tex_id);
	if(state != STATE_PUNCHLEFT && state != STATE_JUMPPUNCHLEFT && state != STATE_SHOOTLEFT && state != STATE_JUMPSHOOTLEFT){
	  glBegin(GL_QUADS);	
		  glTexCoord2f(xo,yo);	glVertex2i(screen_x  ,screen_y);
		  glTexCoord2f(xf,yo);	glVertex2i(screen_x+w,screen_y);
		  glTexCoord2f(xf,yf);	glVertex2i(screen_x+w,screen_y+h);
		  glTexCoord2f(xo,yf);	glVertex2i(screen_x  ,screen_y+h);
	  glEnd();
	}
	else {
	  glBegin(GL_QUADS);	
		  glTexCoord2f(xo,yo);	glVertex2i(screen_x-6  ,screen_y);
		  glTexCoord2f(xf,yo);	glVertex2i(screen_x+w-6,screen_y);
		  glTexCoord2f(xf,yf);	glVertex2i(screen_x+w-6,screen_y+h);
		  glTexCoord2f(xo,yf);	glVertex2i(screen_x-6  ,screen_y+h);
	  glEnd();
	}

	glDisable(GL_TEXTURE_2D);
}

void cBicho::MoveLeft(cTile* map)
{
    float xaux;
    
    xaux = xf;
    if (onWater) {
        waterSpeed -= acceleration;
        if(waterSpeed <= -2) waterSpeed = -2;
        xf += waterSpeed;
    }
    else xf -= moveSpeed;
    x = ceil(xf);
        
    if(onWater && CollidesMapWall(map,true) && waterSpeed > 0){
        xf = xaux;
        waterSpeed -= acceleration;
	x = ceil(xf);
    }

    if(CollidesMapWall(map,false))
    {
        xf = xaux;
        state = STATE_LOOKLEFT;
	x = ceil(xf);
    }
    
    else
    {
        if((state == STATE_PUNCHLEFT || state == STATE_PUNCHRIGHT || state == STATE_SHOOTLEFT || state == STATE_SHOOTRIGHT) && cont > 0) {
            --cont;
        }
        else if(!onWater && !jumping && state != STATE_WALKLEFT && state != STATE_FANFARE)
        {
            state = STATE_WALKLEFT;
            seq = 0;
            delay = 0;
        }
        
        if(state == STATE_JUMPRIGHT) state = STATE_JUMPLEFT;
	
	if(onWater) {
		    switch(state) {
			case STATE_JUMPPUNCHLEFT: 
			if(cont == 0) state = STATE_JUMPLEFT; 
			else --cont;
			break;
		      case STATE_JUMPPUNCHRIGHT: 
			if(cont == 0) state = STATE_JUMPRIGHT; 
			else --cont;
			break;
		      case STATE_JUMPSHOOTLEFT: 
			if(cont == 0) state = STATE_JUMPLEFT; 
			else --cont;
			break;
		      case STATE_JUMPSHOOTRIGHT: 
			if(cont == 0) state = STATE_JUMPRIGHT; 
			else --cont;
			break;
		    }
	}
    }
}
void cBicho::MoveRight(cTile* map)
{
    float xaux = xf;
    
    if (onWater) {
	  waterSpeed += acceleration;
	  if(waterSpeed >= 2) waterSpeed = 2;
	  xf += waterSpeed;
    }
    else xf += moveSpeed;
    
    if(onWater && CollidesMapWall(map,false) && waterSpeed < 0){
	  xf = xaux;
	  waterSpeed += acceleration;
    }
    
    if(CollidesMapWall(map,true))
    {
        xf = xaux;
        if(state == STATE_WALKRIGHT) state = STATE_LOOKRIGHT;
    }
	//Advance, no problem
	else
	{
		if((state == STATE_PUNCHLEFT || state == STATE_PUNCHRIGHT || state == STATE_SHOOTLEFT || state == STATE_SHOOTRIGHT) && cont > 0) {
		  --cont;
		}
		
		else if(!onWater && !jumping && state != STATE_WALKRIGHT && state != STATE_FANFARE)
		{
			state = STATE_WALKRIGHT;
			seq = 0;
			delay = 0;
		}
		
		if(state == STATE_JUMPLEFT) state = STATE_JUMPRIGHT;
		
		if(onWater) {
		    switch(state) {
			case STATE_JUMPPUNCHLEFT: 
			if(cont == 0) state = STATE_JUMPLEFT; 
			else --cont;
			break;
		      case STATE_JUMPPUNCHRIGHT: 
			if(cont == 0) state = STATE_JUMPRIGHT; 
			else --cont;
			break;
		      case STATE_JUMPSHOOTLEFT: 
			if(cont == 0) state = STATE_JUMPLEFT; 
			else --cont;
			break;
		      case STATE_JUMPSHOOTRIGHT: 
			if(cont == 0) state = STATE_JUMPRIGHT; 
			else --cont;
			break;
		    }
		}
	}
}

void cBicho::Atack(cTile* map)
{
	if(state != STATE_FANFARE){
        //Reiniciem contador de frame.
        cont = 4;
        //canviem state.
        switch(state) {
            case STATE_WALKLEFT: state = STATE_PUNCHLEFT; break;
            case STATE_WALKRIGHT: state = STATE_PUNCHRIGHT; break;
            case STATE_LOOKLEFT: state = STATE_PUNCHLEFT; break;
            case STATE_LOOKRIGHT: state = STATE_PUNCHRIGHT; break; 
            case STATE_JUMPLEFT: state = STATE_JUMPPUNCHLEFT; break;
            case STATE_JUMPRIGHT: state = STATE_JUMPPUNCHRIGHT; break;
        }
        if(!shoot_mode) {
            PunchCollisions(map, state == STATE_PUNCHRIGHT || state == STATE_JUMPPUNCHRIGHT,
                            state == STATE_JUMPPUNCHRIGHT || state == STATE_JUMPPUNCHLEFT);
        }
        else ShotBullet(map); 
	}
}
void cBicho::ShotBullet(cTile* map)
{
    switch(state) {
      case STATE_PUNCHLEFT: state = STATE_SHOOTLEFT; break;
      case STATE_PUNCHRIGHT: state = STATE_SHOOTRIGHT; break;
      case STATE_JUMPPUNCHLEFT: state = STATE_JUMPSHOOTLEFT; break;
      case STATE_JUMPPUNCHRIGHT: state = STATE_JUMPSHOOTRIGHT; break;
    }
    cont += 4;
  
    cBullet* bullet = new cBullet(scene);
    bool right = state == STATE_SHOOTRIGHT || state == STATE_JUMPSHOOTRIGHT;
    bullet->x = x+(right?w:-8); bullet->y = y+9; bullet->right = right; 
    bullet->tex_id = tex_id;
    entities->push_back(bullet);
}
void cBicho::Stop()
{
	waterSpeed = 0;

	if(!jumping && state != STATE_FANFARE) {
	  switch(state)
	  {
		  case STATE_WALKLEFT:	state = STATE_LOOKLEFT;		break;
		  case STATE_WALKRIGHT:	state = STATE_LOOKRIGHT;	break;
		  case STATE_PUNCHLEFT:   
		    if(cont == 0) state = STATE_LOOKLEFT;
		    else --cont;
		    break;
		  case STATE_PUNCHRIGHT : 	
		    if(cont == 0) state = STATE_LOOKRIGHT; 
		    else --cont;
		    break;
		  case STATE_JUMPRIGHT: state = STATE_LOOKRIGHT; break;
		  case STATE_JUMPLEFT: state = STATE_LOOKLEFT; break;
		  case STATE_JUMPPUNCHRIGHT:
		    if(cont == 0) state = STATE_LOOKRIGHT;
		    else --cont;
		    break;
		  case STATE_JUMPPUNCHLEFT:
		    if(cont == 0) state = STATE_LOOKLEFT; 
		    else --cont;
		    break;
		  case STATE_SHOOTLEFT:   
		    if(cont == 0) state = STATE_LOOKLEFT;
		    else --cont;
		    break;
		  case STATE_SHOOTRIGHT : 	
		    if(cont == 0) state = STATE_LOOKRIGHT; 
		    else --cont;
		    break;
		  case STATE_JUMPSHOOTRIGHT:
		    if(cont == 0) state = STATE_LOOKRIGHT;
		    else --cont;
		    break;
		  case STATE_JUMPSHOOTLEFT:
		    if(cont == 0) state = STATE_LOOKLEFT; 
		    else --cont;
		    break;  
		    
	  }
	}

}
void cBicho::MoveDown(cTile* map) {
    if(onWater) {
      if(CollidesMapFloor(map)) y -= moveSpeed;
      else y -= 2*moveSpeed;
      
      switch(state) {
	case STATE_LOOKLEFT:
	  state = STATE_JUMPLEFT;
	  break;
	case STATE_LOOKRIGHT:
	  state = STATE_JUMPRIGHT;
	  break;
	case STATE_PUNCHLEFT:
	  state = STATE_JUMPPUNCHLEFT;
	  break;
	case STATE_PUNCHRIGHT:
	  state = STATE_JUMPPUNCHRIGHT;
	  break;
	case STATE_SHOOTLEFT:
	  state = STATE_JUMPSHOOTLEFT;
	  break;
	case STATE_SHOOTRIGHT:
	  state = STATE_JUMPSHOOTRIGHT;
	  break;
      }
    }
}

void cBicho::Jump(cTile *map)
{
	if(!jumping && CollidesMapFloor(map)) {
        jumping = true;
        jump_alfa = 0;
        jump_y = y;
        cSoundManager::getInstance()->PlaySound("jump");
    }
	
	if(jumping) {// state != STATE_JUMPLEFT || state != STATE_JUMPRIGHT*/) {
	  switch(state) {
	    case STATE_LOOKLEFT: state = STATE_JUMPLEFT; break;
	    case STATE_LOOKRIGHT: state = STATE_JUMPRIGHT; break;
	    case STATE_WALKLEFT: state = STATE_JUMPLEFT; break;
	    case STATE_WALKRIGHT: state = STATE_JUMPRIGHT; break;
	    case STATE_PUNCHLEFT: 
	      if(cont == 0) state = STATE_JUMPLEFT; 
	      else --cont;
	      break;
	    case STATE_PUNCHRIGHT: 
	      if(cont == 0) state = STATE_JUMPRIGHT; 
	      else --cont;
	      break;
	    case STATE_JUMPPUNCHLEFT: 
	      if(cont == 0) state = STATE_JUMPLEFT; 
	      else --cont;
	      break;
	    case STATE_JUMPPUNCHRIGHT: 
	      if(cont == 0) state = STATE_JUMPRIGHT; 
	      else --cont;
	      break;
	    case STATE_SHOOTLEFT: 
	      if(cont == 0) state = STATE_JUMPLEFT; 
	      else --cont;
	      break;
	    case STATE_SHOOTRIGHT: 
	      if(cont == 0) state = STATE_JUMPRIGHT; 
	      else --cont;
	      break;
	    case STATE_JUMPSHOOTLEFT: 
	      if(cont == 0) state = STATE_JUMPLEFT; 
	      else --cont;
	      break;
	    case STATE_JUMPSHOOTRIGHT: 
	      if(cont == 0) state = STATE_JUMPRIGHT; 
	      else --cont;
	      break;  
	  }
	}
}
void cBicho::setOnWater(bool value)
{
    onWater = value;
    moveSpeed = value ? 1 : STEP_LENGTH;
    jumpSpeed = value ? 1 : JUMP_STEP;
    xf = x;
    yf = 0;
}
void cBicho::Logic(cTile* map)
{
	float alfa;
    
	if(jumping)
	{
		jump_alfa += jumpSpeed;
		
		if(jump_alfa == 180)
		{
			jumping = false;
            ceil_length = -1;
			y = jump_y;
		}
		else
		{
			alfa = ((float)jump_alfa) * 0.017453f;
            if(ceil_length != -1)
                y = jump_y + (int)( ((float)ceil_length) * sin(alfa) );
            else 
                y = jump_y + (int)( ((float)JUMP_HEIGHT) * sin(alfa) );
		
			//Over floor?
            if(jump_alfa > 90 && CollidesMapFloor(map)) {
                jumping = false;
                ceil_length = -1;
            }
            if(CollidesMapCeiling(map)) {
                jump_alfa = 90;
                ceil_length = y - jump_y;
            }
		}
		
		switch(state) {
		    case STATE_JUMPPUNCHLEFT: 
		    if(cont == 0) {
                state = STATE_JUMPLEFT; 
            }
		    else --cont;
		    break;
		  case STATE_JUMPPUNCHRIGHT: 
		    if(cont == 0) {
                state = STATE_JUMPRIGHT; 
            }
		    else --cont;
		    break;
		    case STATE_JUMPSHOOTLEFT: 
		    if(cont == 0) {
                state = STATE_JUMPLEFT; 
            }
		    else --cont;
		    break;
		  case STATE_JUMPSHOOTRIGHT: 
		    if(cont == 0) {
                state = STATE_JUMPRIGHT; 
            }
		    else --cont;
		    break;
		  }
	}
	else
	{
		//Over floor?
		if(!CollidesMapFloor(map)){
		  y -= (2*moveSpeed);
		  
		  //Si estem caien també posem animació de salt.
		  switch(state) {
		    case STATE_LOOKLEFT: state = STATE_JUMPLEFT; break;
		    case STATE_LOOKRIGHT: state = STATE_JUMPRIGHT; break;
		    case STATE_WALKLEFT: state = STATE_JUMPLEFT; break;
		    case STATE_WALKRIGHT: state = STATE_JUMPRIGHT; break;
		    case STATE_PUNCHLEFT: state = STATE_JUMPPUNCHLEFT; break;
		    case STATE_PUNCHRIGHT: state = STATE_JUMPPUNCHRIGHT; break;
		    case STATE_JUMPPUNCHLEFT: state = STATE_JUMPPUNCHLEFT; break;
		    case STATE_JUMPPUNCHRIGHT: state = STATE_JUMPPUNCHRIGHT; break;
		    case STATE_SHOOTLEFT: state = STATE_JUMPSHOOTLEFT; break;
		    case STATE_SHOOTRIGHT: state = STATE_JUMPSHOOTRIGHT; break;
		    case STATE_JUMPSHOOTLEFT: state = STATE_JUMPSHOOTLEFT; break;
		    case STATE_JUMPSHOOTRIGHT: state = STATE_JUMPSHOOTRIGHT; break;
		  }
		}
	}
}
void cBicho::NextFrame(int max)
{
	delay++;
	if(delay == FRAME_DELAY)
	{
		seq++;
		seq%=max;
		delay = 0;
	}
}
int cBicho::GetFrame()
{
	return seq;
}
int cBicho::GetState()
{
	return state;
}
void cBicho::SetState(int s)
{
	state = s;
}
int cBicho::GetCont() { return cont;}