#include "cDyingBlock.h"

cDyingBlock::cDyingBlock(int tile_id)
{
    this->tile_id = tile_id;
    frame = 0;
    w = h = 16;
}
cDyingBlock::~cDyingBlock()
{
    
}
void cDyingBlock::draw()
{
    ++frame;
    if(frame % 2 == 0) return;
    if(frame > 10) {destroy(); return;}
    
    float norm_size_x = 1/(float)TILESET_WIDTH;
    float norm_size_y = 1/(float)TILESET_HEIGHT;
    float coordx_tile = (tile_id % TILESET_WIDTH)*norm_size_x;
    float coordy_tile = (tile_id / TILESET_WIDTH)*norm_size_y;
    int px = x+SCENE_Xo;
    int py = y+SCENE_Yo;
    
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, cData::getInstance()->GetID(IMG_BLOCKS));
    glBegin(GL_QUADS);
        glTexCoord2f(coordx_tile,coordy_tile+norm_size_y); glVertex2i(px,py);
        glTexCoord2f(coordx_tile+norm_size_x,coordy_tile+norm_size_y); glVertex2i(px+w,py);
        glTexCoord2f(coordx_tile+norm_size_x,coordy_tile); glVertex2i(px+w,py+h);
        glTexCoord2f(coordx_tile,coordy_tile); glVertex2i(px,py+h);
    glEnd();
}
