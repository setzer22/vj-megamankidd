#include "cGamba.h"

cGamba::cGamba(cScene* scene_) {
  w = 24; h = 24;
  vx = 1.5;
  dir = 1;
  cont = -1; //el -1 representa que está viu.
  scene = scene_;
}
cGamba::~cGamba() {}

void cGamba::draw()
{
    //Draws the sprite
    if(cont == -1 || cont%2 == 0){ //si la gamba no está morta, o está els frames parells de la seva mort es pinta, sino no.
    float xo,yo,xf,yf;
    xf = yf = 0;
    
    float sqy = 0.5f; 
    float sqx = 0.333f;
    
    xo = (propulsor)?0:sqx; yo = ((dir == -1)?1:2)* sqy;
	propulsor = !propulsor;
    xf = xf + xo + sqx;
    yf = yf + yo - sqy;
    
    int screen_x,screen_y;

    screen_x = x + SCENE_Xo;
    screen_y = y + SCENE_Yo;

    glEnable(GL_TEXTURE_2D);
    
    glBindTexture(GL_TEXTURE_2D,tex_id);
    glBegin(GL_QUADS);	
	    glTexCoord2f(xo,yo);	glVertex2i(screen_x  ,screen_y);
	    glTexCoord2f(xf,yo);	glVertex2i(screen_x+w,screen_y);
	    glTexCoord2f(xf,yf);	glVertex2i(screen_x+w,screen_y+h);
	    glTexCoord2f(xo,yf);	glVertex2i(screen_x  ,screen_y+h);
    glEnd();
    }
}
void cGamba::input(unsigned char* keys)
{
    //check for the keys and modify vx, vy
    //if it's an enemy, AI goes here
  
}
void cGamba::update()
{
    //update according to vx, vy. This blindly moves the character
    //and checks for collisons with the environment. Doesn't use AI
    if (cont == -1) {
      if(collidesWithMap()) dir = -dir;
      x += (vx*dir);
    }
    else {
      if(cont == 0) destroy();
      else --cont;
    }
}
void cGamba::onCollision(cEntity* e)
{
    //switch case with e->getType(), check for the player, bullets and so on
    switch(e->getType()){
      case ENTITY_BULLET: //si ens dona una bala morim.
	if(cont == -1) dead();
        break;
    case ENTITY_PUNY:
	  if(cont == -1 && static_cast<cPuny*>(e)->attacking) dead();
	  break;
    }
}

bool cGamba::collidesWithMap() {
    int tx = (x + (dir == 1?w:0) ) / TILE_SIZE, ty = y / TILE_SIZE;
    cTile* map = scene->GetMap();
    if(map[ tx + (ty*SCENE_WIDTH) ].collider) return true;
    return false;
}

void cGamba::dead()
{
  vx = 0;
  cont = 20;
  points += 50;
}
