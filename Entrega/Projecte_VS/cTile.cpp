#include "cTile.h"
#include <iostream>
using namespace std;

cTile::cTile() {
    collider = false;
    ceiling = false;
    harmful = false;
    destroyable = false;
    has_points = false;
}
cTile::cTile(int tile_id_, int x_, int y_, std::vector< cEntity* >* entities)
{
    tile_id = tile_id_;
    x = x_;
    y = y_;
    collider = false;
    ceiling = false;
    harmful = false;
    destroyable = false;
    has_points = false;
    this->entities = entities;
    Data = cData::getInstance();
}
bool cTile::onAttack()
{
    if(destroyable) {
        //Spawn a dying block where this tile is
        cDyingBlock* b = new cDyingBlock(tile_id);
        b->x = x*16; b->y = y*16; 
        entities->push_back(static_cast<cEntity*>(b));
        
        tile_id = -1;
		destroyable = false;
        collider = false;
        ceiling = false;
	
        if(cheats_always_points || has_points) {
            cPepita* Pepita = new cPepita(); 
            Pepita->x = 16*x;
            Pepita->y = 16*y;
            Pepita->tex_id = Data->GetID(IMG_PEPITA);
            entities->push_back(static_cast<cEntity*>(Pepita));
        }
        return true; //This means update the scene because it's been modified
    }
    return false;
}
