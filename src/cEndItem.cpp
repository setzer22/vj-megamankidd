#include "cEndItem.h"
#include "cGame.h"

cEndItem::cEndItem() {
    eaten = false;
    w = 12;
    h = 13;
}
void cEndItem::draw()
{
    if(!eaten) { 
        float xo = 0, yo = 1, yf = 0 , xf = 1;
        float screen_x = x+SCENE_Xo, screen_y = y+SCENE_Yo;
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D,tex_id);
        glBegin(GL_QUADS);  
            glTexCoord2f(xo,yo);    glVertex2i(screen_x  ,screen_y);
            glTexCoord2f(xf,yo);    glVertex2i(screen_x+w,screen_y);
            glTexCoord2f(xf,yf);    glVertex2i(screen_x+w,screen_y+h);
            glTexCoord2f(xo,yf);    glVertex2i(screen_x  ,screen_y+h);
        glEnd();
    }
}
int cEndItem::getType()
{
    return ENTITY_RICEBALL;
}
void cEndItem::onCollision(cEntity* e)
{
    if(e->getType() == ENTITY_PLAYER) {
	static_cast<cBicho*>(e)->SetState(STATE_FANFARE);
        eaten = true;
    }
}
void cEndItem::update() {}
void cEndItem::input(unsigned char* keys) {}
