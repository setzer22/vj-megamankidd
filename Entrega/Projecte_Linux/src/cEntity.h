#pragma once
#include "Globals.h"

#define ENTITY_PLAYER 0
#define ENTITY_BULLET 1
#define ENTITY_ENEMY 2
#define ENTITY_GAMBA 3
#define ENTITY_BUBBLE 5
#define ENTITY_CRANC 6
#define ENTITY_RICEBALL 7
#define ENTITY_PUNY 8
#define ENTITY_BUBBLEMAN 9
#define ENTITY_HITBOX 10
#define ENTITY_RPS  11
#define ENTITY_PEPITA 12
#define ENTITY_POWERUP 13
#define ENTITY_NAVI 14
#define ENTITY_NAVI_HITBOX 15
#define ENTITY_DYINGBLOCK 16

#define SCENE_Xo        (2*TILE_SIZE)
#define SCENE_Yo        TILE_SIZE

#define SCENE_WIDTH    100
#define SCENE_HEIGHT   100

#define TILE_SIZE       16
#define BLOCK_SIZE      16  


class cEntity {
public:
    cEntity();
    cEntity(int tex_id, int x, int y);
    virtual ~cEntity();
    
    virtual void draw() = 0;
    virtual void update() = 0;
    virtual void input(unsigned char* keys) = 0;
    virtual void onCollision(cEntity* e) = 0;
    void destroy() {toDestroy = true;}
    bool needsDestroy() {return toDestroy;}
    virtual int getType() = 0;
    
    int x, y, w, h;
    int tex_id;
private:
    bool toDestroy;
};