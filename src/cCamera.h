#pragma once

#include "cScene.h"
#include "cPlayer.h"

class cCamera {
public:
	
	cCamera();
	~cCamera();
	
	void Update(cPlayer* player, cScene* scene);
	
	double x, y, width, heigh;
	double offset_x, offset_y;
};