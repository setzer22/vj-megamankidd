#pragma once

#include "cGameState.h"
#include "cSoundManager.h"
#include "cData.h"

class cGameStateMenu : public cGameState {
public:
    cGameStateMenu(vector< cEntity* >* entities, unsigned char* keys);
    virtual ~cGameStateMenu();
    
    virtual bool Init() override;
    virtual bool Loop() override;
    virtual void Render() override;
    bool Process();
    
private:
    vector<cEntity*>* entities;
    unsigned char* keys;
    int count;
};

#include "cGame.h"