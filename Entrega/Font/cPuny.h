#pragma once

#include "Globals.h"
#include "cEntity.h"

class cPuny : public cEntity {
public:
    cPuny();
    virtual ~cPuny();
    
    virtual void draw() override;
    virtual void update() override {}
    virtual void input(unsigned char* keys) override {}
    virtual void onCollision(cEntity* e) override;
    virtual int getType() {return ENTITY_PUNY;}
    
    bool attacking; 
};