#pragma once

#include "Globals.h"
#include "cEntity.h"
#include "cScene.h"
#include "cTile.h"

class cBullet : public cEntity {
public:
    cBullet(cScene* scene);
    ~cBullet();
    
    virtual void draw() override;
    virtual void update() override;
    virtual void input(unsigned char* keys) override {}
    virtual void onCollision(cEntity* e) override;
    virtual int getType() {return ENTITY_BULLET;}
    bool collidesWithMap();
    
    bool right;
    int speed;    
    cScene* scene;
};