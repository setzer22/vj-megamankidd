#include "cBubble.h"

cBubble::cBubble(cScene* scene_) {
    w = 16;
    h = 16;
    speed = 2;
    scene = scene_;
}

cBubble::~cBubble() {}

void cBubble::draw() {
    float sqx = 0.3333333f, sqy = 0.5f;
    float xo = 0.0, yo = sqy+0.4, yf = 0.5 , xf = sqx;
    float screen_x = x+SCENE_Xo, screen_y = y+SCENE_Yo;
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D,tex_id);
    glBegin(GL_QUADS);  
        glTexCoord2f(xo,yo);    glVertex2i(screen_x  ,screen_y);
        glTexCoord2f(xf,yo);    glVertex2i(screen_x+w,screen_y);
        glTexCoord2f(xf,yf);    glVertex2i(screen_x+w,screen_y+h);
        glTexCoord2f(xo,yf);    glVertex2i(screen_x  ,screen_y+h);
    glEnd();
}

bool cBubble::collidesWithMap() {
    int ty = (y + h) / TILE_SIZE, tx = x / TILE_SIZE;
    cTile* map = scene->GetMap();
    if(map[ tx + (ty*SCENE_WIDTH) ].collider) {
        bool needsRefresh = map[ tx + (ty*SCENE_WIDTH) ].onAttack();
        if(needsRefresh) scene->UpdateDisplay();
        return true;   
    }
    return false;
}

void cBubble::update() {
    y += speed;
    if(collidesWithMap()) destroy();
    if(y > SCENE_HEIGHT * TILE_SIZE || y < 0) destroy();
}

void cBubble::onCollision(cEntity* e) {
  //si no es un enemic, o la navi, explota
  if(e->getType() != ENTITY_GAMBA &&
     e->getType() != ENTITY_CRANC && 
     e->getType() != ENTITY_PUNY  &&
     e->getType() != ENTITY_NAVI  &&
     e->getType() != ENTITY_NAVI_HITBOX) {
      
        destroy();
  }
}