#include "cGameStatePlay.h"

cGameStatePlay::cGameStatePlay(vector< cEntity* >* entities, unsigned char* keys): cGameState(entities, keys)
{
    this->entities = entities;
    this->keys = keys;
    Scene = new cScene(entities);
    SoundManager = cSoundManager::getInstance();
    Data = cData::getInstance();
    camX = 0, camY = 0; 
    endLevelCount = END_LEVEL_DELAY;
}
cGameStatePlay::~cGameStatePlay()
{
    delete Scene;
}
bool cGameStatePlay::Init()
{
    bool res = true;
    
    SoundManager->LoadSound("level2.ogg", "willy");
    SoundManager->LoadSound("end.ogg", "end");
    SoundManager->LoadSound("jump.ogg","jump");
    SoundManager->PlaySound("willy");

    //Scene initialization
    res = Data->LoadImage(IMG_BLOCKS,"blocks.png",GL_RGBA);
    if(!res) return false;
    res = Data->LoadImage(IMG_BACKGROUND,"background.png",GL_RGBA);
    if(!res) return false;
    res = Data->LoadImage(IMG_BACKGROUND_WATER,"backgroundwater.png",GL_RGBA);
    if(!res) return false;
    res = Scene->LoadLevel(1);
    if(!res) return false;
    res = Data->LoadImage(IMG_PEPITA, "pepita.png", GL_RGBA);
    if(!res) return false;

    //Player initialization
    res = Data->LoadImage(IMG_PLAYER,"megaman.png",GL_RGBA);
    if(!res) return false;
    Player = new cPlayer(entities, Scene);
    Player->SetWidthHeight(24,24);
    Player->SetTile(2,93);
    Player->SetState(STATE_LOOKRIGHT);
    Player->tex_id = Data->GetID(IMG_PLAYER);
    entities->push_back(static_cast<cEntity*>(Player));
    
    res = Data->LoadImage(IMG_NAVI, "navi.png", GL_RGBA);
    if(!res) return false;
    cNavi* navi = new cNavi(Player, entities);
    navi->setXY(2*16, 93*16);
    navi->tex_id = Data->GetID(IMG_NAVI);
    entities->push_back(static_cast<cEntity*>(navi));
    
    res = Data->LoadImage(IMG_RICEBALL, "riceball.png", GL_RGBA);
    if(!res) return false;
    BolaArros = new cEndItem(); 
    BolaArros->x = 16*91;
    BolaArros->y = 16*1;
    BolaArros->tex_id = Data->GetID(IMG_RICEBALL);
    entities->push_back(static_cast<cEntity*>(BolaArros));
    
    /*Creació dels enemics*/
    
    res = Data->LoadImage(IMG_GAMBA,"gama.png",GL_RGBA);
    if(!res) return false;
    
    cGamba* TestGamba = new cGamba(Scene);
    TestGamba->x = 16*20;
    TestGamba->y = 16*93;
    TestGamba->tex_id = Data->GetID(IMG_GAMBA);
  
    cGamba* TestGamba2 = new cGamba(Scene);
    TestGamba2->x = 16*25; TestGamba2->y = 16*82; TestGamba2->tex_id = Data->GetID(IMG_GAMBA);
    
    cGamba* g1 = new cGamba(Scene);
    g1->x = 16*3; g1->y = 16*11; g1->tex_id = Data->GetID(IMG_GAMBA);
    
    cGamba* g2 = new cGamba(Scene);
    g2->x = 16*87; g2->y = 16*9; g2->tex_id = Data->GetID(IMG_GAMBA);
    
    cGamba* g3 = new cGamba(Scene);
    g3->x = 16*32; g3->y = 16*3; g3->tex_id = Data->GetID(IMG_GAMBA);
    
    cGamba* g4 = new cGamba(Scene);
    g4->x = 16*32; g4->y = 16*10; g4->tex_id = Data->GetID(IMG_GAMBA);
    
    cGamba* g5 = new cGamba(Scene);
    g5->x = 16*5; g5->y = 16*47; g5->tex_id = Data->GetID(IMG_GAMBA);
    
    cGamba* g6 = new cGamba(Scene);
    g6->x = 16*21; g6->y = 16*26; g6->tex_id = Data->GetID(IMG_GAMBA);
    
    entities->push_back(static_cast<cEntity*>(TestGamba));
    entities->push_back(static_cast<cEntity*>(TestGamba2));
    entities->push_back(static_cast<cEntity*>(g1));
    entities->push_back(static_cast<cEntity*>(g2));
    entities->push_back(static_cast<cEntity*>(g3));
    entities->push_back(static_cast<cEntity*>(g4));
    entities->push_back(static_cast<cEntity*>(g5));
    entities->push_back(static_cast<cEntity*>(g6));
    
    res = Data->LoadImage(IMG_CRANC,"cranc.png",GL_RGBA);
    if(!res) return false;
    
    cCranc* TestCranc = new cCranc(entities,Scene);
    TestCranc->x = 16*32; TestCranc->y = 16*57.9; TestCranc->tex_id = Data->GetID(IMG_CRANC);
    entities->push_back(static_cast<cEntity*>(TestCranc));
    
    cCranc* TestCranc2 = new cCranc(entities,Scene);
    TestCranc2->x = 16*19; TestCranc2->y = 16*93; TestCranc2->tex_id = Data->GetID(IMG_CRANC);
    entities->push_back(static_cast<cEntity*>(TestCranc2));
    
    cCranc* TestCranc3 = new cCranc(entities,Scene);
    TestCranc3->x = 16*12.7; TestCranc3->y = 16; TestCranc3->tex_id = Data->GetID(IMG_CRANC);
    entities->push_back(static_cast<cEntity*>(TestCranc3));
    
    cCranc* c1 = new cCranc(entities,Scene);
    c1->x = 16*63.7; c1->y = 16; c1->tex_id = Data->GetID(IMG_CRANC);
    
    cCranc* c2 = new cCranc(entities,Scene);
    c2->x = 16*26; c2->y = 16; c2->tex_id = Data->GetID(IMG_CRANC);
    
    entities->push_back(static_cast<cEntity*>(c1));
    entities->push_back(static_cast<cEntity*>(c2));
    
    /*fi creació d'enemics*/
    
    /*Creació de pepites especials*/
    cPepita* p = new cPepita();
    p->x = 16; p->y = 16;
    p->tex_id = Data->GetID(IMG_PEPITA);
    entities->push_back(static_cast<cEntity*>(p));
    
    cPepita* p2 = new cPepita();
    p2->x = 16*2; p2->y = 16*33; 
    p2->tex_id = Data->GetID(IMG_PEPITA);
    entities->push_back(static_cast<cEntity*>(p2));
    
    cPepita* p3 = new cPepita();
    p3->x = 16*72; p3->y = 16*11;
    p3->tex_id = Data->GetID(IMG_PEPITA);
    entities->push_back(static_cast<cEntity*>(p3));
    /*fi creació pepites*/
    
    /*Crean l'anell que dona l'habilitat de disparar*/
    cPowerup* Ring = new cPowerup();
    Ring->x = 16*23;
    Ring->y = 16*64;
    Ring->tex_id = Data->GetID(IMG_RING);
    entities->push_back(static_cast<cEntity*>(Ring));
    /************************************************/
    
    return res;
}
bool cGameStatePlay::Loop()
{
    bool res = true; 
    
    if(Player->GetState() == STATE_DEAD && Player->GetDeadCont() == 0) {
      cGame::ChangeState(GAMESTATE_GAMEOVER,1);
    }
    
    if(BolaArros->isEaten()) {
        if(endLevelCount == END_LEVEL_DELAY) {
            cSoundManager::getInstance()->StopSound("willy");
            cSoundManager::getInstance()->PlaySound("end");
	    points += 500;//Guanyem 500 punts per pasarnos el primer nivell.
        }
        --endLevelCount;
        if(endLevelCount <= 0) {
            cGame::ChangeState(GAMESTATE_LEVEL2);
        }
    }
    
    res = Process();
    if(!res) return res;
    
    EntityCollisions();
    for (int i = 0; i < entities->size(); ++i) {
        if((*entities)[i]->needsDestroy()) {
            cEntity* to_destroy = (*entities)[i];
            (*entities)[i] = (*entities)[entities->size()-1];
            (*entities).pop_back();
            delete to_destroy;
            ++i;
        }
    }
    glutPostRedisplay();
    return res;
}
bool  cGameStatePlay::Process()
{
    bool res = true;
    if(keys[27]) exit(0);
    
    Camera.Update(Player,Scene);
    
    for(int i = 0; i < entities->size(); ++i) {
        (*entities)[i]->input(keys); 
    }
    for(int i = 0; i < entities->size(); ++i) {
        (*entities)[i]->update();
    }
    return res;
}
void cGameStatePlay::Render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    
    
    if(Player->getOnWater()) Scene->DrawBackground(Data->GetID(IMG_BACKGROUND_WATER)); 
    else Scene->DrawBackground(Data->GetID(IMG_BACKGROUND));
    glLoadIdentity();
    glScaled(2,2,2);
    glTranslated(Camera.x, Camera.y, 0);
    Scene->Draw(Data->GetID(IMG_BLOCKS));
    for(int i = 0; i < entities->size(); ++i) {
        (*entities)[i]->draw(); 
        //drawRect(entities[i]); 
    }
    
    glLoadIdentity();
    RenderGUI();
    
    glutSwapBuffers();
}
void cGameStatePlay::RenderGUI() {
    // Score
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, Data->GetID(IMG_GUI));
    glBegin(GL_QUADS);
        glTexCoord2f(0,1); glVertex3f(GAME_WIDTH - 135 + 0 , GAME_HEIGHT - 59 + 0, 0);
        glTexCoord2f(1,1); glVertex3f(GAME_WIDTH - 135 + 100, GAME_HEIGHT - 59 + 0, 0);
        glTexCoord2f(1,0); glVertex3f(GAME_WIDTH - 135 + 100, GAME_HEIGHT - 59 + 30, 0);
        glTexCoord2f(0,0); glVertex3f(GAME_WIDTH - 135 + 0, GAME_HEIGHT - 59 + 30, 0);
    glEnd();
    glDisable(GL_TEXTURE_2D);
	//We use long long because microsoft thought that variable had to be very long indeed
    string s = to_string((long long)points);
    while(s.length() < 4) s = "0"+s;
    for(int i = 0; i < s.length(); ++i) {
        glRasterPos2f((GAME_WIDTH - 95) + (12*i), GAME_HEIGHT - 50);
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, s[i]);
    }
    // Powerups
    string s2 = "punch mode";
    if(Player->GetShootMode()) s2 = "shoot mode";
    for(int i = 0; i < s2.length(); ++i) {
        glRasterPos2f(21+(12*i), GAME_HEIGHT - 50);
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, s2[i]);
    }
     
    // Creative
}


bool cGameStatePlay::collide(cEntity* e1, cEntity* e2) 
{
    int ax1 = e1->x, ax2 = e1->x + e1->w, bx1 = e2->x, bx2 = e2->x+e2->w;
    int ay1 = e1->y, ay2 = e1->y + e1->h, by1 = e2->y, by2 = e2->y+e2->h;
    return (ax1 < bx2 && ax2 > bx1 && ay1 < by2 && ay2 > by1);
}
void cGameStatePlay::EntityCollisions() 
{
    vector<cEntity*>& entities2 = *entities;
    for(int i = 0; i < entities2.size(); ++i) {
        for(int j = 0; j < i; ++j) {
            if(collide(entities2[i], entities2[j])) {
                (entities2[i])->onCollision(entities2[j]);
                (entities2[j])->onCollision(entities2[i]);
            }
        }
    }
}
