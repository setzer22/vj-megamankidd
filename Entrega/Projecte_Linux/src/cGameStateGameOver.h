#pragma once

#include "cGameState.h"
#include "cSoundManager.h"
#include "cData.h"
#include "cGame.h"

class cGameStateGameOver : public cGameState {
public:
    cGameStateGameOver(vector< cEntity* >* entities, unsigned char* keys);
    virtual ~cGameStateGameOver();
    
    virtual bool Init() override;
    virtual bool Loop() override;
    virtual void Render() override;
    bool Process();
    void setLevel(int level);
    
private:
    vector<cEntity*>* entities;
    unsigned char* keys;
    int level;
};