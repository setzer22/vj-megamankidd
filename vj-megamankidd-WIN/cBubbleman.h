#pragma once 

#include "Globals.h"
#include "cEntity.h"

#define ANIM_LENGTH 9

class cBubbleman : public cEntity {
public:
    cBubbleman();
    virtual ~cBubbleman();

    virtual void draw();
    virtual int getType();
    virtual void input(unsigned char* keys);
    virtual void onCollision(cEntity* e);
    virtual void update();
    void countToThree();
    void getReady();
    void die();
    
private:
    int state;
    int count;
    bool counting;
    int dead;
};