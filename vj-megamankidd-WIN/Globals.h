#pragma once

#ifndef LINUX
    #include <windows.h>
    #include <stdlib.h>
    #include <gl/glut.h>
#else
    #include <GL/gl.h>
    #include <GL/glut.h>
#endif

#include <math.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <vector>
#include <map>

#define GAME_WIDTH  640
#define GAME_HEIGHT 480

#define TILESET_WIDTH 5
#define TILESET_HEIGHT 5

#define TILE_SIZE       16
#define BLOCK_SIZE      16  

#define END_LEVEL_DELAY 400

#define GAMESTATE_MENU      0
#define GAMESTATE_GAME      1
#define GAMESTATE_INSTR     2
#define GAMESTATE_CREDITS   3
#define GAMESTATE_LEVEL2    4
#define GAMESTATE_GAMEOVER  5

#define GLUT_KEY_X 120
#define GLUT_KEY_C 99

extern int points;
extern bool cheats_always_ring;
extern bool cheats_always_points;
extern bool cheats_immortal;
