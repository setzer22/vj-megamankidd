#include "cBicho.h"
#include "cScene.h"
#include "Globals.h"

#include "cCamera.h"

cCamera::cCamera() {
	x = 0, y = 0, width = 0, heigh = 0;
	offset_x = 308/2, offset_y = 228/2;
}

cCamera::~cCamera() {}

//Dummy behaviour, just follows the player
void cCamera::Update(cPlayer* player, cScene* scene) {
	int px, py; player->GetPosition(&px, &py);
	x = offset_x - (double)px;
	y = offset_y - (double)py;
}	