#pragma once

#include "Globals.h"
#include "cEntity.h"

using namespace std;

class cPepita: public cEntity {
public:
    cPepita();
    virtual ~cPepita() {}
    
    virtual void draw();
    virtual int getType();
    virtual void input(unsigned char* keys);
    virtual void onCollision(cEntity* e);
    virtual void update();
};