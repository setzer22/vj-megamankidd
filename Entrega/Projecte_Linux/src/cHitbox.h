#include "Globals.h"
#include "cEntity.h"

class cHitbox : public cEntity {
public:
    cHitbox();
    virtual ~cHitbox();
    
    virtual void draw();
    virtual int getType() {return ENTITY_HITBOX;}
    virtual void input(unsigned char* keys);
    virtual void onCollision(cEntity* e); 
    virtual void update();
    
    int id;
};