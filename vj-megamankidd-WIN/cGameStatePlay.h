#pragma once

#include "cGameState.h"
#include "cSoundManager.h"
#include "cData.h"

#include "cGame.h"
#include "cScene.h"
#include "cPlayer.h"
#include "cGamba.h"
#include "cBullet.h"
#include "cCamera.h"
#include "cEntity.h"
#include "cCranc.h"
#include "cBubble.h"
#include "cEndItem.h"
#include "cBubbleman.h"
#include "cPepita.h"
#include "cNavi.h"

class cGameStatePlay : public cGameState {
public:
    cGameStatePlay(vector< cEntity* >* entities, unsigned char* keys);
    virtual ~cGameStatePlay();
    
    virtual bool Init() override;
    virtual bool Loop() override;
    virtual void Render() override;
    bool Process();
    void EntityCollisions();
    bool collide(cEntity* e1, cEntity* e2);
    bool loadEnemies();
    void RenderGUI();
    
private:
    int endLevelCount;
    cScene* Scene;
    cPlayer* Player;
    cEndItem* BolaArros;
    cCamera Camera;
    double camX, camY;
    
    cSoundManager* SoundManager;
    cData* Data;
    
    vector<cEntity*>* entities;
    unsigned char* keys;
};