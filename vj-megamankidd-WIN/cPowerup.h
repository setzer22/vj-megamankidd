#pragma once

#include "Globals.h"
#include "cEntity.h"

using namespace std;

class cPowerup: public cEntity {
public:
    cPowerup();
    virtual ~cPowerup() {}
    
    virtual void draw();
    virtual int getType();
    virtual void input(unsigned char* keys);
    virtual void onCollision(cEntity* e);
    virtual void update();
    
    cEntity* player;
};