#pragma once

#include "cData.h"
#include "cSoundManager.h"

#include "cGameState.h"
#include "cGameStatePlay.h"
#include "cGameStatePlay2.h"
#include "cGameStateMenu.h"
#include "cGameStateCredits.h"
#include "cGameStateHelp.h"
#include "cGameStateGameOver.h"

#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

#define MAX_ENTITIES 100

class cGame
{
public:
	cGame(void);
	virtual ~cGame(void);

	static bool Init();
	static bool Loop();
	static void Finalize();

	//Input
	static void ReadKeyboard(unsigned char key, int x, int y, bool press);
	static void ReadMouse(int button, int state, int x, int y);
	static void Render();
    
	
	static void drawRect(cEntity* e);
    
    static void ChangeState(int state, int level=0);
    
private:
    bool collide(cEntity* e1, cEntity* e2);
    
    static int state;
    static unsigned char keys[256];
    static vector<cEntity*> entities;
	
    static cGameState* level1State;
    static cGameState* level2State;
    static cGameState* menuState;
    static cGameState* creditsState;
    static cGameState* helpState;
    static cGameState* gameOver;
    
};
