#pragma once

#include "cGameState.h"
#include "cSoundManager.h"
#include "cData.h"

class cGameStateHelp : public cGameState {
public:
    cGameStateHelp(vector< cEntity* >* entities, unsigned char* keys);
    virtual ~cGameStateHelp();
    
    virtual bool Init() override;
    virtual bool Loop() override;
    virtual void Render() override;
    bool Process();
    
private:
    vector<cEntity*>* entities;
    unsigned char* keys;
    int count;
};

#include "cGame.h"
