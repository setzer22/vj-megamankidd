
#include "cGameStatePlay2.h"

int cGameStatePlay2::rpsPool[10] = {ROCK, ROCK, PAPER, SCISSORS, PAPER, PAPER, SCISSORS, PAPER, ROCK, SCISSORS};

// 0: tie, 1: megaman wins, -1: megaman looses
int cGameStatePlay2::winner[3][3] = { {0, 1, -1}, {-1, 0, 1}, {1, -1, 0} };

cGameStatePlay2::cGameStatePlay2(vector< cEntity* >* entities, unsigned char* keys): cGameState(entities, keys)
{
    this->entities = entities;
    this->keys = keys;
    Scene = new cScene(entities);
    SoundManager = cSoundManager::getInstance();
    Data = cData::getInstance();
    camX = 0, camY = 0;
    endLevelCount = END_LEVEL_DELAY;
    bossfight = endbossfight = false;
    bossfight_count = 0;
    rpsIndex = 0;
    megamanPoints = bubblemanPoints = 0;
    lastResult = false;
}
cGameStatePlay2::~cGameStatePlay2()
{
    delete Scene;
}
bool cGameStatePlay2::Init()
{
    bool res = true;
    
    SoundManager->LoadSound("test.ogg", "level2");
    SoundManager->LoadSound("rockpaperscissors.ogg", "rps");
    SoundManager->LoadSound("end.ogg", "end");
    SoundManager->LoadSound("jump.ogg","jump");
    SoundManager->PlaySound("level2");

    //Scene initialization
    res = Data->LoadImage(IMG_BLOCKS,"blocks2.png",GL_RGBA);
    if(!res) return false;
    res = Data->LoadImage(IMG_BACKGROUND2,"background2.png",GL_RGBA);
    if(!res) return false;
    res = Scene->LoadLevel(2);
    if(!res) return false;
    res = Data->LoadImage(IMG_PEPITA, "pepita.png", GL_RGBA);
    if(!res) return false;

    //Player initialization
    res = Data->LoadImage(IMG_PLAYER,"megaman.png",GL_RGBA);
    if(!res) return false;
    Player = new cPlayer(entities, Scene);
    Player->SetWidthHeight(24,24);
    Player->SetTile(2,87);
    //Player->SetTile(59,35);
    Player->SetState(STATE_LOOKRIGHT);
    Player->tex_id = Data->GetID(IMG_PLAYER);
    entities->push_back(static_cast<cEntity*>(Player));

	res = Data->LoadImage(IMG_NAVI, "navi.png", GL_RGBA);
    if(!res) return false;
    cNavi* navi = new cNavi(Player, entities);
    navi->setXY(2*16, 93*16);
    navi->tex_id = Data->GetID(IMG_NAVI);
    entities->push_back(static_cast<cEntity*>(navi));
    
    res = Data->LoadImage(IMG_RPS, "rockpaperscissors.png", GL_RGBA);
    if(!res) return false;
    /*rps = new cRPS();
    rps->x = (61*16); rps->y = (5*16);  rps->tex_id = Data->GetID(IMG_RPS);
    Player->setRPS(rps);
    entities->push_back(static_cast<cEntity*>(rps));*/
    
    res = Data->LoadImage(IMG_BUBBLEMAN,"bubbleman.png",GL_RGBA);
    if(!res) return false;
    bubbleman = new cBubbleman();
    bubbleman->x = 76*16; bubbleman->y = 1*16; bubbleman->tex_id = Data->GetID(IMG_BUBBLEMAN);
    entities->push_back(static_cast<cEntity*>(bubbleman));
    
    cHitbox* cutscene = new cHitbox();
    cutscene->x = 61*16; cutscene->y = 24*16; cutscene->w = 5*16; cutscene->h = 5*16;
    cutscene->id = 0;
    entities->push_back(static_cast<cEntity*>(cutscene));
    
     /*Creació dels enemics*/
    res = Data->LoadImage(IMG_GAMBA,"gama.png",GL_RGBA);
    if(!res) return false;
    
  
    cGamba* g = new cGamba(Scene);
    g->x = 16*92; g->y = 16*89; g->tex_id = Data->GetID(IMG_GAMBA);
    
    cGamba* g1 = new cGamba(Scene);
    g1->x = 16*90; g1->y = 16*61; g1->tex_id = Data->GetID(IMG_GAMBA);
    
    cGamba* g2 = new cGamba(Scene);
    g2->x = 16*87; g2->y = 16*52; g2->tex_id = Data->GetID(IMG_GAMBA);
    
    cGamba* g3 = new cGamba(Scene);
    g3->x = 16*32; g3->y = 16*52; g3->tex_id = Data->GetID(IMG_GAMBA);
    
    cGamba* g4 = new cGamba(Scene);
    g4->x = 16*10; g4->y = 16*18; g4->tex_id = Data->GetID(IMG_GAMBA);
    
    
    entities->push_back(static_cast<cEntity*>(g));
    entities->push_back(static_cast<cEntity*>(g1));
    entities->push_back(static_cast<cEntity*>(g2));
    entities->push_back(static_cast<cEntity*>(g3));
    entities->push_back(static_cast<cEntity*>(g4));
    
    res = Data->LoadImage(IMG_CRANC,"cranc.png",GL_RGBA);
    if(!res) return false;
    
    cCranc* c = new cCranc(entities,Scene);
    c->x = 16*15; c->y = 16*87; c->tex_id = Data->GetID(IMG_CRANC);
    entities->push_back(static_cast<cEntity*>(c));
    
    cCranc* c2 = new cCranc(entities,Scene);
    c2->x = 16*84; c2->y = 16*77; c2->tex_id = Data->GetID(IMG_CRANC);
    entities->push_back(static_cast<cEntity*>(c2));
    
    cCranc* c3 = new cCranc(entities,Scene);
    c3->x = 16*19; c3->y = 16*29; c3->tex_id = Data->GetID(IMG_CRANC);
    entities->push_back(static_cast<cEntity*>(c3));
    
    cCranc* c4 = new cCranc(entities,Scene);
    c4->x = 16*6; c4->y = 16*37; c4->tex_id = Data->GetID(IMG_CRANC);
    
    cCranc* c5 = new cCranc(entities,Scene);
    c5->x = 16*31; c5->y = 16*23; c5->tex_id = Data->GetID(IMG_CRANC);
    
    entities->push_back(static_cast<cEntity*>(c4));
    entities->push_back(static_cast<cEntity*>(c5));
    /*fi creació d'enemics*/
    
    /*Creació de pepites especials*/
    for(int i = 0; i < 5; ++i) {//Tresor
      cPepita* pepita = new cPepita();
      pepita->tex_id = Data->GetID(IMG_PEPITA);
      pepita->x = 16*80; pepita->y = 16*53;
      entities->push_back(static_cast<cEntity*>(pepita));
    }
    
    cPepita* p1 = new cPepita();
    p1->x = 16*25; p1->y = 16*32; p1->tex_id = Data->GetID(IMG_PEPITA);
    entities->push_back(static_cast<cEntity*>(p1));
    /*fi creació pepites*/
    
    /*Crean l'anell que dona l'habilitat de disparar*/
    cPowerup* Ring = new cPowerup();
    Ring->x = 16*17;
    Ring->y = 16*87;
    Ring->tex_id = Data->GetID(IMG_RING);
    entities->push_back(static_cast<cEntity*>(Ring));
    /************************************************/
    
    return res;
}
bool cGameStatePlay2::Loop()
{
    bool res = true; 
    
    res = Process();
    if(!res) return res;
    
    EntityCollisions();
    for (int i = 0; i < entities->size(); ++i) {
        if((*entities)[i]->needsDestroy()) {
            cout << "needs destroy " << i << endl;
            cEntity* to_destroy = (*entities)[i];
            (*entities)[i] = (*entities)[entities->size()-1];
            (*entities).pop_back();
            delete to_destroy;
            ++i;
        }
    }
    glutPostRedisplay();
    return res;
}
bool  cGameStatePlay2::Process()
{
    bool res = true;
    if(keys[27]) exit(0);
    
    if(Player->GetState() == STATE_DEAD && Player->GetDeadCont() == 0) {
      cGame::ChangeState(GAMESTATE_GAMEOVER,2);
    }
    
    int cx = Camera.x;
    Camera.Update(Player,Scene);
    
    for(int i = 0; i < entities->size(); ++i) {
        (*entities)[i]->input(keys); 
    }
    for(int i = 0; i < entities->size(); ++i) {
        (*entities)[i]->update();
    }
    if(!bossfight && !endbossfight && Player->GetBossfight()) {
        bossfight = true;
    }
    if(bossfight) {
        
        if(megamanPoints == 2) {
            Player->SetState(STATE_FANFARE); 
            bubbleman->die();
            endbossfight = true;
            bossfight = false; bossfight_count = 0;
        }
        else if(bubblemanPoints == 2) {
            Player->SetState(STATE_DEAD);
            endbossfight = true;
            bossfight = false; bossfight_count = 0;
        }
        
        ++bossfight_count; 
        Camera.x = cx;
        if(Camera.y > 10) Camera.y = 10;
        //Timeline
            //Intro
        if(bossfight_count < 310 && bossfight_count % 2 == 0) Camera.x -= 1;
        if(bossfight_count == 360) {
			SoundManager->StopSound("level2");
			bubbleman->getReady();
		}
            //Match
        int t2 = bossfight_count - 500; if(t2 < 0) return true;
        int t = t2 % 860;
        if(t == 0) {
			SoundManager->PlaySound("rps");
			rps = new cRPS();
			rps->x = (Player->x); rps->y = (5*16);  rps->tex_id = Data->GetID(IMG_RPS);
			Player->setRPS(rps);
			entities->push_back(static_cast<cEntity*>(rps));
			rps->visible = true;
		}
        if(t == 500) bubbleman->countToThree();
        if(t == 560) {
			SoundManager->StopSound("rps");
            rps2 = new cRPS();
            rps2->tex_id = Data->GetID(IMG_RPS);
            int result = rpsPool[rpsIndex]; rpsIndex = (rpsIndex+1)%10;
            rps2->visible = true; rps2->x = 77*16; rps2->y = 5*16; rps2->set(result);
            entities->push_back(static_cast<cEntity*>(rps2));
            
            int won = whoWon();
            if(won == 1) ++megamanPoints;
            else if(won == -1) ++bubblemanPoints;
            lastResult = won;
        }
        if(t == 760) {
            rps->visible = false;
            rps2->visible = false;
        }
    }
    
    if(endbossfight) {
        Camera.x = cx;
        if(Camera.y > 10) Camera.y = 10;
        
        ++bossfight_count;
        
        if(megamanPoints == 2) {Player->SetState(STATE_FANFARE);}
        else if(bubblemanPoints == 2) {bubbleman->countToThree(); Player->SetState(STATE_DEAD);}
        
        if(bossfight_count == 1000) {
            cGame::ChangeState(GAMESTATE_MENU);
        }
    }
    
    return res;
}
int cGameStatePlay2::whoWon() {
    int r1 = rps->get();
    int r2 = rps2->get();
    return winner[r1][r2];
}
void cGameStatePlay2::Render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    
    Scene->DrawBackground(Data->GetID(IMG_BACKGROUND2));
    glScaled(2,2,2);
    glTranslated(Camera.x, Camera.y, 0);
    Scene->Draw(Data->GetID(IMG_BLOCKS));
    for(int i = 0; i < entities->size(); ++i) {
        (*entities)[i]->draw(); 
        //drawRect(entities[i]); 
    }
    
    glLoadIdentity();
    RenderGUI();
    
    glutSwapBuffers();
}
void cGameStatePlay2::RenderGUI()
{
    // Score
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, Data->GetID(IMG_GUI));
    glBegin(GL_QUADS);
        glTexCoord2f(0,1); glVertex3f(GAME_WIDTH - 135 + 0 , GAME_HEIGHT - 59 + 0, 0);
        glTexCoord2f(1,1); glVertex3f(GAME_WIDTH - 135 + 100, GAME_HEIGHT - 59 + 0, 0);
        glTexCoord2f(1,0); glVertex3f(GAME_WIDTH - 135 + 100, GAME_HEIGHT - 59 + 30, 0);
        glTexCoord2f(0,0); glVertex3f(GAME_WIDTH - 135 + 0, GAME_HEIGHT - 59 + 30, 0);
    glEnd();
    glDisable(GL_TEXTURE_2D);
	//We use long long because microsoft thought that variable had to be very long indeed
    string s = to_string((long long)points);
    while(s.length() < 4) s = "0"+s;
    for(int i = 0; i < s.length(); ++i) {
        glRasterPos2f((GAME_WIDTH - 95) + (12*i), GAME_HEIGHT - 50);
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, s[i]);
    }
    // Powerups
    string s2 = "punch mode";
    if(Player->GetShootMode()) s2 = "shoot mode";
    for(int i = 0; i < s2.length(); ++i) {
        glRasterPos2f(21+(12*i), GAME_HEIGHT - 50);
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, s2[i]);
    }
     
    // Creative
}

bool cGameStatePlay2::collide(cEntity* e1, cEntity* e2) 
{
    int ax1 = e1->x, ax2 = e1->x + e1->w, bx1 = e2->x, bx2 = e2->x+e2->w;
    int ay1 = e1->y, ay2 = e1->y + e1->h, by1 = e2->y, by2 = e2->y+e2->h;
    return (ax1 < bx2 && ax2 > bx1 && ay1 < by2 && ay2 > by1);
}
void cGameStatePlay2::EntityCollisions() 
{
    vector<cEntity*>& entities2 = *entities;
    for(int i = 0; i < entities2.size(); ++i) {
        for(int j = 0; j < i; ++j) {
            if(collide(entities2[i], entities2[j])) {
                (entities2[i])->onCollision(entities2[j]);
                (entities2[j])->onCollision(entities2[i]);
            }
        }
    }
}
