#pragma once

#include "cBicho.h"

#define PLAYER_START_CX		96
#define PLAYER_START_CY		5

class cPlayer: public cBicho
{
public:
	cPlayer(vector<cEntity*>* entities, cScene* scene);
	~cPlayer();
    
    void draw() override;
};
