#pragma once

#include "Globals.h"
#include "cEntity.h"

using namespace std;

class cEndItem : public cEntity {
public:
    cEndItem();
    virtual ~cEndItem() {}
    
    virtual void draw();
    virtual int getType();
    virtual void input(unsigned char* keys);
    virtual void onCollision(cEntity* e);
    virtual void update();
    bool isEaten() {return eaten;}
    
private:
    bool eaten;
};