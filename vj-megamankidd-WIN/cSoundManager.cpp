#ifndef LINUX
#include "cSoundManager.h"



using namespace std;
using namespace irrklang;

#pragma comment(lib, "irrKlang.lib")

cSoundManager* cSoundManager::instance = NULL;

cSoundManager::cSoundManager()
{
	engine = createIrrKlangDevice();
}
cSoundManager::~cSoundManager()
{
}
void cSoundManager::LoadSound(string path, string name)
{
	nameToFile[name] = path;
}
void cSoundManager::PlaySound(string name, bool looping)
{
	engine->play2D(nameToFile[name].c_str(), looping);
}
void cSoundManager::StopSound(string name)
{
	engine->stopAllSounds();
}

#else 

#include "cSoundManager.h"
using namespace std;

cSoundManager* cSoundManager::instance = NULL;

cSoundManager::cSoundManager()
{
}
cSoundManager::~cSoundManager()
{
}
void cSoundManager::LoadSound(string path, string name)
{
}
void cSoundManager::PlaySound(string name, bool looping)
{
}
void cSoundManager::StopSound(string name)
{
}

#endif