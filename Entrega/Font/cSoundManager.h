#pragma once

#include "Globals.h"
#ifndef LINUX
  #include <irrKlang.h>
#endif

class cSoundManager {
public:
    
    cSoundManager();
    ~cSoundManager();
    
    //Loads a song from path and stores it with the given name
    void LoadSound(std::string path, std::string name);
    
    //Plays the sound with the given name
    void PlaySound(std::string name, bool looping = true);
    
    //Stops the sound with the given name 
    void StopSound(std::string name);
    
    //Singleton pattern 
    static cSoundManager* getInstance() {
        if(!instance) instance = new cSoundManager();
        return instance;
    }
    
private:
	static cSoundManager* instance; 
	std::map<std::string, std::string> nameToFile;
#ifndef LINUX
	irrklang::ISoundEngine* engine; 
#endif
};