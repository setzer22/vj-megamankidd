#include "cRPS.h"

cRPS::cRPS() {
    state = ROCK;
    w = 30, h = 30;
    visible = false;
}
void cRPS::draw() {
    if(!visible) return;
    float sqx = 0.3333333f, sqy = 1;
    float xo = state*sqx, yo = 1, yf = 0 , xf = xo+sqx;
    float screen_x = x+SCENE_Xo, screen_y = y+SCENE_Yo;
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D,tex_id);
    glBegin(GL_QUADS);  
        glTexCoord2f(xo,yo);    glVertex2i(screen_x  ,screen_y);
        glTexCoord2f(xf,yo);    glVertex2i(screen_x+w,screen_y);
        glTexCoord2f(xf,yf);    glVertex2i(screen_x+w,screen_y+h);
        glTexCoord2f(xo,yf);    glVertex2i(screen_x  ,screen_y+h);
    glEnd();
}
cRPS::~cRPS() {}
int cRPS::getType() {return ENTITY_RPS;}
void cRPS::input(unsigned char* keys) {
    if(keys['n']) {
        state = (state+1)%3;
    }
}
void cRPS::next() { state = (state+1)%3; }
void cRPS::previous() { if(--state < 0) state = 2; }
    
void cRPS::onCollision(cEntity* e) {}
void cRPS::update() {}