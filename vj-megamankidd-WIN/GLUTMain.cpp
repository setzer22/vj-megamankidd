
#include "Globals.h"
#include "cGame.h"

#include <iostream>
using namespace std;

//Delete console
#ifndef LINUX
#pragma comment(linker, "/subsystem:\"windows\" /entry:\"mainCRTStartup\"")
#endif

int startTime;

//Global initializations
int points = 0;
bool cheats_always_ring = false;
bool cheats_always_points = false;
bool cheats_immortal = false;

cGame Game;

void AppRender()
{
	cGame::Render();
}
void AppKeyboard(unsigned char key, int x, int y)
{
	cGame::ReadKeyboard(key,x,y,true);
}
void AppKeyboardUp(unsigned char key, int x, int y)
{
	cGame::ReadKeyboard(key,x,y,false);
}
void AppSpecialKeys(int key, int x, int y)
{
	cGame::ReadKeyboard(key,x,y,true);
}
void AppSpecialKeysUp(int key, int x, int y)
{
	cGame::ReadKeyboard(key,x,y,false);
}
void AppMouse(int button, int state, int x, int y)
{
	cGame::ReadMouse(button,state,x,y);
}
void AppIdle()
{
	if(glutGet(GLUT_ELAPSED_TIME) - startTime > 1000/60)
	{
		startTime = glutGet(GLUT_ELAPSED_TIME);
		if(!Game.Loop()) exit(0);
	}
}

#ifdef LINUX
int main(int argc, char** argv)
#else
void main(int argc, char** argv)
#endif
{
    points = 0;
	int res_x,res_y,pos_x,pos_y;
	//GLUT initialization
	glutInit(&argc, argv);

	//RGBA with double buffer
	glutInitDisplayMode(GLUT_RGBA | GLUT_ALPHA | GLUT_DOUBLE);

	//Create centered window
	res_x = glutGet(GLUT_SCREEN_WIDTH);
	res_y = glutGet(GLUT_SCREEN_HEIGHT);
	pos_x = (res_x>>1)-(GAME_WIDTH>>1);
	pos_y = (res_y>>1)-(GAME_HEIGHT>>1);
	
	glutInitWindowPosition(pos_x,pos_y);
	glutInitWindowSize(GAME_WIDTH,GAME_HEIGHT);
	glutCreateWindow("Bubble returns!");

	/*glutGameModeString("800x600:32");
	glutEnterGameMode();*/

	//Make the default cursor disappear
	//glutSetCursor(GLUT_CURSOR_NONE);

	//Game initializations
	Game.Init();

	//Register callback functions
	glutDisplayFunc(AppRender);			
	glutKeyboardFunc(AppKeyboard);		
	glutKeyboardUpFunc(AppKeyboardUp);	
	glutSpecialFunc(AppSpecialKeys);	
	glutSpecialUpFunc(AppSpecialKeysUp);
	glutMouseFunc(AppMouse);
	glutIdleFunc(AppIdle);

	startTime = glutGet(GLUT_ELAPSED_TIME);
	//Application loop
	glutMainLoop();	
}
