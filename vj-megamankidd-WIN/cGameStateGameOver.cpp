#include "cGameStateGameOver.h"

cGameStateGameOver::cGameStateGameOver(std::vector< cEntity* >* entities, unsigned char* keys): cGameState(entities, keys)
{
  this->entities = entities;
  this->keys = keys;
  this->level = 1;
}

cGameStateGameOver::~cGameStateGameOver() {}

bool cGameStateGameOver::Init()
{
  bool res = true;
  res = cData::getInstance()->LoadImage(IMG_GAMEOVER, "gameover.png", GL_RGBA);
  if(!res) return false;

  cSoundManager::getInstance()->StopSound("level2");
  
  return res;
}

bool cGameStateGameOver::Loop()
{
  cout << "GAME OVER!" << endl;
  Process();
  glutPostRedisplay();
  return true;
}

bool cGameStateGameOver::Process()
{
  if(keys[27]) {
    keys[27] = false;
    cGame::ChangeState(GAMESTATE_MENU);
  }
  
  if(keys[13] && level == 1) {
    keys[13] = false;
    points = 0;
    cGame::ChangeState(GAMESTATE_GAME);
  }
  else if(keys[13] && level == 2) {
    keys[13] = false;
    points = 0;
    cGame::ChangeState(GAMESTATE_LEVEL2);
  }
  
  return true;
  
}

void cGameStateGameOver::Render()
{
  cout << "rendering game over" << endl;
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, cData::getInstance()->GetID(IMG_GAMEOVER));
    glBegin(GL_QUADS);
        glTexCoord2f(0,1); glVertex3f(0,0,0);
        glTexCoord2f(0,0); glVertex3f(0, GAME_HEIGHT, 0);
        glTexCoord2f(1,0); glVertex3f(GAME_WIDTH, GAME_HEIGHT, 0);
        glTexCoord2f(1,1); glVertex3f(GAME_WIDTH, 0, 0);
    glEnd();
    glDisable(GL_TEXTURE_2D);
    
    string s = to_string((long long)points);
    for(int i = 0; i < s.length(); ++i) {
        glRasterPos2f(GAME_WIDTH/2 + (12*i) + 42, GAME_HEIGHT/2 - 21);
        glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, s[i]);
    }
    
    glutSwapBuffers();
    
}

void cGameStateGameOver::setLevel(int level)
{
  this->level = level;
}


