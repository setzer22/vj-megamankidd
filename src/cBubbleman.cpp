#include "cBubbleman.h"

cBubbleman::cBubbleman() {
    w = h = 30;
    state = count = 0;
    counting = false;
    dead = false;
}
 
cBubbleman::~cBubbleman() {}

void cBubbleman::draw() {
    if(!dead || count % 2 == 0) {
        float sqx = 0.3333333f, sqy = 1;
        float xo = state*sqx, yo = 1, yf = 0 , xf = xo+sqx;
        float screen_x = x+SCENE_Xo, screen_y = y+SCENE_Yo;
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D,tex_id);
        glBegin(GL_QUADS);  
            glTexCoord2f(xo,yo);    glVertex2i(screen_x  ,screen_y);
            glTexCoord2f(xf,yo);    glVertex2i(screen_x+w,screen_y);
            glTexCoord2f(xf,yf);    glVertex2i(screen_x+w,screen_y+h);
            glTexCoord2f(xo,yf);    glVertex2i(screen_x  ,screen_y+h);
        glEnd();
    }
}
int cBubbleman::getType() {
    return ENTITY_BUBBLEMAN;
}
void cBubbleman::input(unsigned char* keys) {
    if(keys['C'] || keys['c']) countToThree();
}
void cBubbleman::onCollision(cEntity* e) {

}
void cBubbleman::update() {
    if(counting) {
        ++count;
        int c = count/ANIM_LENGTH;
        if(c < 6) {
            if(c < 1) state = 0;
            else if(c < 2) state = 1;
            else if(c < 3) state = 0;
            else if(c < 4) state = 1;
            else if(c < 5) state = 0;
            else if(c < 6) state = 1;
        }
        else {
            counting = false;
            count = 0;
            state = 2;
        }
    }
    if (dead) {
        ++count;
        if(count > 120) destroy();
    }
}
void cBubbleman::countToThree() {
    counting = true; 
}
void cBubbleman::getReady() {
    state = 2;
}
void cBubbleman::die() {
    std::cout << "die bubbleman, die" << std::endl;
    dead = true;
    count = 0;
}

