#include "cGameStateMenu.h"

cGameStateMenu::cGameStateMenu(std::vector< cEntity* >* entities, unsigned char* keys): cGameState(entities, keys)
{
    this->entities = entities;
    this->keys = keys;
    count = 0;
}
cGameStateMenu::~cGameStateMenu()
{
}
bool cGameStateMenu::Init() {
    bool res = false;;
    res = cData::getInstance()->LoadImage(IMG_MENU, "menu.png", GL_RGBA);
    if(!res) return false;
    res = cData::getInstance()->LoadImage(IMG_START, "start.png", GL_RGBA);
    if(!res) return false;
    
    //Preload the powerup images here so both levels have them
    res = cData::getInstance()->LoadImage(IMG_RING, "ring.png", GL_RGBA);
    if(!res) return false;
    res = cData::getInstance()->LoadImage(IMG_PEPITA, "pepita.png", GL_RGBA);
    if(!res) return false;
    res = cData::getInstance()->LoadImage(IMG_GUI, "gui_pepitas.png", GL_RGBA);
    if(!res) return false;
    
    cSoundManager::getInstance()->LoadSound("menu.ogg","menu");
    cSoundManager::getInstance()->PlaySound("menu", true);
    return res;
}
bool cGameStateMenu::Loop()
{
    cout << count << endl;
    Process();
    glutPostRedisplay();
    return true;
}
bool cGameStateMenu::Process()
{
    if(keys[27]) exit(0);
    else if(keys[13]) {
        cSoundManager::getInstance()->StopSound("menu");
	points = 0;
        cGame::ChangeState(GAMESTATE_GAME);
    }
    else if(keys['2']) {
        cSoundManager::getInstance()->StopSound("menu");
	points = 0;
        cGame::ChangeState(GAMESTATE_LEVEL2);
    }
    else if(keys['c'] || keys['C']) {
        cSoundManager::getInstance()->StopSound("menu");
        cGame::ChangeState(GAMESTATE_CREDITS);
    }
    else if(keys['h'] || keys['H']) {
        cSoundManager::getInstance()->StopSound("menu");
        cGame::ChangeState(GAMESTATE_INSTR);
    }
    return true;
}
void cGameStateMenu::Render()
{
    count = (count+1)%90;
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, cData::getInstance()->GetID(IMG_MENU));
    glBegin(GL_QUADS);
        glTexCoord2f(0,1); glVertex3f(0,0,0);
        glTexCoord2f(0,0); glVertex3f(0, GAME_HEIGHT, 0);
        glTexCoord2f(1,0); glVertex3f(GAME_WIDTH, GAME_HEIGHT, 0);
        glTexCoord2f(1,1); glVertex3f(GAME_WIDTH, 0, 0);
    glEnd();
    if(count < 45) {
        glBindTexture(GL_TEXTURE_2D, cData::getInstance()->GetID(IMG_START));
        glBegin(GL_QUADS);
            glTexCoord2f(0,1); glVertex3f(145,GAME_HEIGHT-457,-0.5);
            glTexCoord2f(0,0); glVertex3f(145, GAME_HEIGHT-411, -0.5);
            glTexCoord2f(1,0); glVertex3f(485, GAME_HEIGHT-411, -0.5);
            glTexCoord2f(1,1); glVertex3f(485, GAME_HEIGHT-457, -0.5);
        glEnd();
    }
    glutSwapBuffers();
}
