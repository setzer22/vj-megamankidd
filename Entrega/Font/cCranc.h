#pragma once

#include "Globals.h"
#include "cEntity.h"
#include "cScene.h"
#include "cBubble.h"
#include "cPuny.h"

#include <vector>
#include <iostream>

class cCranc : public cEntity {
public:
    cCranc(vector<cEntity*>* entities_,cScene* scene_);
    ~cCranc();
    
    void draw() override; //Draws the sprite 
    void input(unsigned char* keys) override; //Handles the input / IA
    void update() override; //Handles the logic
    void onCollision(cEntity* e) override; //Handles collisions with other entities (not the scenery)
    int getType() override {return ENTITY_CRANC;} //Must return the entity type
    bool collidesWithMap();
    
    void dead(); //funció per que la gamba mori.
    
    int cont, frame, attack_anime;

private:
    void shoot();
    
    cScene* scene;
    vector<cEntity*>* entities;
    
};