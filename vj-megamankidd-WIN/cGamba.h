#pragma once

#include "Globals.h"
#include "cEntity.h"
#include "cScene.h"
#include "cPuny.h"

class cGamba : public cEntity {
public:
    cGamba(cScene* scene_);
    ~cGamba();
    
    void draw() override; //Draws the sprite 
    void input(unsigned char* keys) override; //Handles the input / IA
    void update() override; //Handles the logic
    void onCollision(cEntity* e) override; //Handles collisions with other entities (not the scenery)
    int getType() override {return ENTITY_GAMBA;} //Must return the entity type
    bool collidesWithMap();
    
    void dead(); //funció per que la gamba mori.
    
    float vx, vy;
    int dir, cont;
    bool propulsor; 
    
    cScene* scene;
    
};
