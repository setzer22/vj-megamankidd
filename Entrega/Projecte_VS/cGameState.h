#pragma once

#include "Globals.h"
#include "cEntity.h"

using namespace std; 

class cGameState {
public:
    cGameState(vector<cEntity*>* entities, unsigned char* keys);
    virtual ~cGameState();
    
    virtual bool Init() = 0;
    virtual bool Loop() = 0;
    virtual void Render() = 0;
    
private:
};