#include "cScene.h"
#include "Globals.h"

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

cScene::cScene(vector< cEntity* >* entities) : map (SCENE_WIDTH*SCENE_HEIGHT) {
    this->entities = entities;
}

cScene::~cScene() {}

bool cScene::LoadLevel(int level)  
{
    this->level = level;
    
    if(level == 1) levelWater = 17;
    else levelWater = 0;
    
	char file[16];

	if(level<10) sprintf(file,"%s0%d%s",(char *)FILENAME,level,(char *)FILENAME_EXT);
	else		 sprintf(file,"%s%d%s",(char *)FILENAME,level,(char *)FILENAME_EXT);
	

    ifstream file_stream;
	file_stream.open(string(file));
    if(!file_stream.is_open()) return false;
    
	id_DL=glGenLists(1);
	glNewList(id_DL,GL_COMPILE);
		glBegin(GL_QUADS);
        
            string map_line;
			for(int j = SCENE_HEIGHT-1; j >= 0; j--) {
                
                getline(file_stream, map_line);
                
				int px=SCENE_Xo;
				int py=SCENE_Yo+(j*TILE_SIZE);

				for(int i = 0; i < SCENE_WIDTH; i++)
				{
                   int tile_num = parseInt(map_line);
					if(tile_num==-1)
					{
                        cTile t = cTile(-1, i, j, entities);
                        t.ceiling = false; t.collider = false;
						map[(j*SCENE_WIDTH)+i] = t;
					}
					else
					{
                        cTile t = makeTile(tile_num, level, i, j);
                        map[(j*SCENE_WIDTH)+i] = t;
                        
                        if(!isAnimated(tile_num, level)/* tile_num <= 14 || tile_num >= 20*/) {
							
							float norm_size_x = 1/(float)TILESET_WIDTH;
							float norm_size_y = 1/(float)TILESET_HEIGHT;

							float coordx_tile = (tile_num % TILESET_WIDTH)*norm_size_x;
							float coordy_tile = (tile_num / TILESET_WIDTH)*norm_size_y;
							
							glTexCoord2f(coordx_tile,coordy_tile+norm_size_y); glVertex2i(px,py);
							glTexCoord2f(coordx_tile+norm_size_x,coordy_tile+norm_size_y); glVertex2i(px+TILE_SIZE,py);
							glTexCoord2f(coordx_tile+norm_size_x,coordy_tile); glVertex2i(px+BLOCK_SIZE,py+TILE_SIZE);
							glTexCoord2f(coordx_tile,coordy_tile); glVertex2i(px,py+TILE_SIZE);
							
							if(tile_num == ROCK_TILE) {
								
							}
						}
					}
					px+=TILE_SIZE;
				}
			}

		glEnd();
	glEndList();
    
    cout << "[CSCENE] Successfully loaded level: " << file << endl;
	return true;
}
void cScene::DrawBackground(int tex_id)
{
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, tex_id);
    glBegin(GL_QUADS);
        glTexCoord2f(0,1); glVertex3f(0,0,0);
        glTexCoord2f(0,0); glVertex3f(0, GAME_HEIGHT, 0);
        glTexCoord2f(1,0); glVertex3f(GAME_WIDTH, GAME_HEIGHT, 0);
        glTexCoord2f(1,1); glVertex3f(GAME_WIDTH, 0, 0);
    glEnd();
}
void cScene::UpdateDisplay() {
    
    float norm_size_x = 1/(float)TILESET_WIDTH;
    float norm_size_y = 1/(float)TILESET_HEIGHT;
    
    glNewList(id_DL,GL_COMPILE);
    glBegin(GL_QUADS); 
    for(int j = SCENE_HEIGHT-1; j >= 0; j--) {
        int py=SCENE_Yo+(j*TILE_SIZE);
        int px=SCENE_Xo;
        for(int i = 0; i < SCENE_WIDTH; i++, px += TILE_SIZE) {
            int tile_num = map[j*SCENE_WIDTH+i].tile_id;
            if (tile_num == -1 || isAnimated(tile_num, level)) continue;
            float coordx_tile = (tile_num % TILESET_WIDTH)*norm_size_x;
            float coordy_tile = (tile_num / TILESET_WIDTH)*norm_size_y;
            
            glTexCoord2f(coordx_tile,coordy_tile+norm_size_y); glVertex2i(px,py);
            glTexCoord2f(coordx_tile+norm_size_x,coordy_tile+norm_size_y); glVertex2i(px+TILE_SIZE,py);
            glTexCoord2f(coordx_tile+norm_size_x,coordy_tile); glVertex2i(px+BLOCK_SIZE,py+TILE_SIZE);
            glTexCoord2f(coordx_tile,coordy_tile); glVertex2i(px,py+TILE_SIZE);
        }
    }
    glEnd();
    glEndList();
}

void cScene::DrawAnimated()
{
    float norm_size_x = 1/(float)TILESET_WIDTH;
    float norm_size_y = 1/(float)TILESET_HEIGHT;
    
    glBegin(GL_QUADS); 
    for(int j = SCENE_HEIGHT-1; j >= 0; j--) {
        int py=SCENE_Yo+(j*TILE_SIZE);
        int px=SCENE_Xo;
        for(int i = 0; i < SCENE_WIDTH; i++, px += TILE_SIZE) {
            int tile_num = map[j*SCENE_WIDTH+i].tile_id;
            if (tile_num == -1 || !isAnimated(tile_num, level)) continue;
			if(anim > ANIM_TIME / 2) tile_num = tile_num + TILESET_WIDTH;
            float coordx_tile = (tile_num % TILESET_WIDTH)*norm_size_x;
            float coordy_tile = (tile_num / TILESET_WIDTH)*norm_size_y;
            
            glTexCoord2f(coordx_tile,coordy_tile+norm_size_y); glVertex2i(px,py);
            glTexCoord2f(coordx_tile+norm_size_x,coordy_tile+norm_size_y); glVertex2i(px+TILE_SIZE,py);
            glTexCoord2f(coordx_tile+norm_size_x,coordy_tile); glVertex2i(px+BLOCK_SIZE,py+TILE_SIZE);
            glTexCoord2f(coordx_tile,coordy_tile); glVertex2i(px,py+TILE_SIZE);
        }
    }
    glEnd();
	
	anim = (anim+1)%(ANIM_TIME+1);
}
cTile cScene::makeTile(int tile_num, int level, int i, int j)
{
    cTile t = cTile(tile_num, i, j, entities);
    
    
    t.ceiling = true; t.collider = true; t.harmful = false; t.destroyable = false;
    if(level == 1) {
        if(tile_num == 4 || tile_num == 6 || tile_num == 11 || tile_num > 14) {
            t.collider = false;
            t.ceiling = false;
        }
        if(tile_num == 13) t.harmful = true;
        if(tile_num == 9) t.destroyable = true;
        if(tile_num < 2) {
            t.ceiling = false;
        }
        
        if(t.destroyable && i%2 == 0 && j%2 == 0) t.has_points = true;
        else t.has_points = false;
    }
    else if (level == 2) {
        if(tile_num == 4 || tile_num == 12 || tile_num == 13 || tile_num == 16) {
            t.ceiling = false;
            t.collider = false;
            if(tile_num == 16) t.harmful = true;
        }
        if(tile_num == 5 || tile_num == 6) {
            t.ceiling = false;
        }
        if(tile_num == 14) t.destroyable = true;

        if(t.destroyable && i%2 == 0 && j%2 == 0) t.has_points = true;
        else t.has_points = false;
    }
    return t;
}
bool cScene::isAnimated(int tile_id, int level)
{
    if(level == 1) return tile_id >= 15 && tile_id <= 19; 
    else if (level == 2) return tile_id == 16;
    return false;
}
void cScene::Draw(int tex_id)
{
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,tex_id);
	glCallList(id_DL);
	DrawAnimated();
	glDisable(GL_TEXTURE_2D);
}
cTile* cScene::GetMap()
{
	return &map[0];
}
