#include "cNavi.h"

cNavi::cNavi(cEntity* player, vector<cEntity*>* entities)
{
    cSoundManager::getInstance()->LoadSound("hey.ogg","hey");
    cSoundManager::getInstance()->LoadSound("listen.ogg", "listen");

    w = 21; h = 21;
    this->player = player;
    target = player;
    state = NAVI_FLYINGTO;
    animState = 0;
    count = 0;
    xf = yf = vx = vy = ax = ay = x = y = 0;
    
    vx = -0.5;
    fly_speed = 2;
    
    hitbox = new cNaviHitbox();
    entities->push_back(static_cast<cEntity*>(hitbox));
}
cNavi::~cNavi()
{

}
void cNavi::update()
{
    ++count;
    if(count % 10 == 0) {
        animState = 1 - animState;
    }
    
    vx += ax;
    vy += ay;
    
    xf += vx;
    yf += vy;
    
    x = xf;
    y = yf;
    
    hitbox->x = x - hitbox->w/2;
    hitbox->y = y - hitbox->h/2;
}
bool cNavi::checkIfDead(cEntity* e) {
    //This isn't the right approach, but our entity handling doesn't
    //Leave room for any better.
    switch (e->getType()) {
        case ENTITY_CRANC:
            return static_cast<cCranc*>(e)->cont != -1;
            break;
        case ENTITY_GAMBA:
            return static_cast<cGamba*>(e)->cont != -1;
            break;
    }
    return false;
}
void cNavi::input(unsigned char* keys)
{
    //If no targets nearby, go back to player
    if(hitbox->inReach.empty()) target = player;
    
    //If we're going back to the player we don't look for enemies
    else if(state != NAVI_GOINGBACK) {
        //Look for the nearest enemy, if any, and set the target
        float min_dist = 100000; 
        for(int i = 0; i < hitbox->inReach.size(); ++i) {
			//If we're here it means we have a target so we play the sound
			if(target == player) {
				string sound = (count % 3 == 0)?"hey":"listen"; 
				cSoundManager::getInstance()->PlaySound(sound, false);
			}

            cEntity* e = hitbox->inReach[i];
            int dx = (e->x) - x;
            int dy = (e->y) - y;
            float dist = sqrt((float)dx*(float)dx + (float)dy*(float)dy);
            if(dist < min_dist) {
                target = e;
                min_dist = dist;
            }
        }
        int dpx = (player->x + 4) - x;
        int dpy = (player->y + 4) - y;
        //If we're too far go back to the player and ignore enemies till we're there
        float dist_to_player = sqrt((float)dpx*(float)dpx + (float)dpy*(float)dpy);
        if(dist_to_player > 200) {
            target = player;
            ChangeState(NAVI_GOINGBACK);
        }
    }
    
    //Actual "IA"
    int dx = (target->x + 4) - x;
    int dy = (target->y + 4) - y;
	//Ugly casts courtesy of Microsoft Corporation! 
    float dist = sqrt((float)dx*(float)dx + (float)dy*(float)dy);
    switch (state) {

        case NAVI_PLAYER:
            if(dist > 25) ChangeState(NAVI_FLYINGTO);
            vy = 0.7*sin((float)count/10.0f);
            if(count % 90 == 0) vx = -1*vx;
            break;
            
        case NAVI_GOINGBACK: //Same as below
        case NAVI_FLYINGTO:
            if(dist <= 2) ChangeState(NAVI_PLAYER);
            else if(checkIfDead(target)) {
				target = player;
                ChangeState(NAVI_GOINGBACK);
            }
            else {
                vx = fly_speed * dx/dist;
                vy = fly_speed * dy/dist;
            }
            break;
    }
}
void cNavi::ChangeState(int newState)
{
    if(newState == NAVI_FLYINGTO) {
        
    }
    switch (state) {
        case NAVI_FLYINGTO: {
            if(newState == NAVI_PLAYER) {
                vx = 0.3;
            }
        }
    }
    state = newState;
}
void cNavi::draw()
{
    float sqx = 0.5, sqy = 1;
    float xo = animState*sqx, yo = 1, yf = 0, xf = xo+sqx;
    float screen_x = x+SCENE_Xo, screen_y = y+SCENE_Yo;
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D,tex_id);
    glBegin(GL_QUADS);  
        glTexCoord2f(xo,yo);    glVertex2i(screen_x  ,screen_y);
        glTexCoord2f(xf,yo);    glVertex2i(screen_x+w,screen_y);
        glTexCoord2f(xf,yf);    glVertex2i(screen_x+w,screen_y+h);
        glTexCoord2f(xo,yf);    glVertex2i(screen_x  ,screen_y+h);
    glEnd();
}
int cNavi::getType() {
    return ENTITY_NAVI;
}
cNaviHitbox::cNaviHitbox() {
    inReach.reserve(20);    
    w = 250; h = 250;
}
void cNaviHitbox::onCollision(cEntity* e) 
{
    if(e->getType() == ENTITY_CRANC || e->getType() == ENTITY_GAMBA) {
        inReach.push_back(e);
    }
}
//For this to work we must ensure navi's update is called before her hitbox's update.
//Fortunately, navi is adding its own hitbox to the entities vector so this is an 
//invariant until one of the two is destroyed. That's what I meant by poor approach
void cNaviHitbox::update() {
    inReach.clear();    
}
cNaviHitbox::~cNaviHitbox() {}
int cNaviHitbox::getType() {return ENTITY_NAVI_HITBOX;}

//Unused 
void cNavi::onCollision(cEntity* e) {}
void cNaviHitbox::draw() {}
void cNaviHitbox::input(unsigned char* keys) {}
