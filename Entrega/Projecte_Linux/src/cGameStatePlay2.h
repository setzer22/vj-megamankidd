
#pragma once

#include "cGameState.h"
#include "cSoundManager.h"
#include "cData.h"

#include "cGame.h"
#include "cScene.h"
#include "cPlayer.h"
#include "cGamba.h"
#include "cBullet.h"
#include "cCamera.h"
#include "cEntity.h"
#include "cCranc.h"
#include "cBubble.h"
#include "cHitbox.h"
#include "cBubbleman.h"
#include "cRPS.h"

class cGameStatePlay2 : public cGameState {
public:
    cGameStatePlay2(vector< cEntity* >* entities, unsigned char* keys);
    virtual ~cGameStatePlay2();
    
    virtual bool Init() override;
    virtual bool Loop() override;
    virtual void Render() override;
    bool Process();
    void EntityCollisions();
    bool collide(cEntity* e1, cEntity* e2);
    void RenderGUI();
    
private:
    int whoWon();
    
    int endLevelCount;
    cScene* Scene;
    cPlayer* Player;
    cRPS* rps;
    cRPS* rps2;
    cBubbleman* bubbleman;
    cCamera Camera;
    double camX, camY;
    
    cSoundManager* SoundManager;
    cData* Data;
    
    vector<cEntity*>* entities;
    unsigned char* keys;
    bool bossfight, endbossfight;
    int bossfight_count;
    int rpsIndex;
    static int rpsPool[10];
    static int winner[3][3];
    int megamanPoints, bubblemanPoints;
    bool lastResult;
};