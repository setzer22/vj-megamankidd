#pragma once

#include "cTexture.h"
#include "Globals.h"

//Image array size
#define NUM_IMG     19

//Image identifiers
#define IMG_BLOCKS	0
#define IMG_PLAYER	1
#define IMG_GAMBA   2
#define IMG_CRANC   3
#define IMG_MENU    4
#define IMG_START   5
#define IMG_BACKGROUND  6
#define IMG_RICEBALL    7
#define IMG_CREDITS    8    
#define IMG_BUBBLEMAN   9
#define IMG_RPS     10
#define IMG_BACKGROUND2 11
#define IMG_PEPITA 12
#define IMG_RING 13
#define IMG_BACKGROUND_WATER 14
#define IMG_GUI 15
#define IMG_GAMEOVER 16
#define IMG_NAVI 17
#define IMG_INSTRUC 18

class cData
{
public:
	cData(void);
	~cData(void);
    
    static cData* getInstance() {
        if(!instance) instance = new cData();
        return instance;
    } 

	int  GetID(int img);
	void GetSize(int img,int *w,int *h);
	bool LoadImage(int img,char *filename,int type = GL_RGBA);

private:
    static cData* instance;
	cTexture texture[NUM_IMG];
};
